from tkinter import *
import giffing
import sys

class meno:
    def __init__(self,master):
        self.path=sys.path[0]+"/"
        self.bgpic=PhotoImage(file=self.path+"pics/home_page/home.png")
        self.bg=Label(master,bg="white")#,image=self.bgpic)

        self.btn_pics=[[PhotoImage(file=self.path + "pics/home_page/new.png"),
                        PhotoImage(file=self.path + "pics/home_page/new_click.png")],
                       [PhotoImage(file=self.path + "pics/home_page/load.png"),
                        PhotoImage(file=self.path + "pics/home_page/load_click.png")],
                       [PhotoImage(file=self.path + "pics/home_page/setting.png"),
                        PhotoImage(file=self.path + "pics/home_page/setting_click.png")],
                       [PhotoImage(file=self.path + "pics/home_page/help.png"),
                        PhotoImage(file=self.path + "pics/home_page/help_click.png")]]
        self.btns=[Label(self.bg, image=self.btn_pics[0][0],bg="white"),
                   Label(self.bg, image=self.btn_pics[1][0],bg="white"),
                   Label(self.bg, image=self.btn_pics[2][0],bg="white"),
                   Label(self.bg, image=self.btn_pics[3][0],bg="white")]
        self.btns[0].place(x=76, y=85)
        self.btns[1].place(x=550, y=85)
        self.btns[2].place(x=76, y=430)
        self.btns[3].place(x=550, y=430)

        self.btns[0].bind("<Button-1>", self.new)
        self.btns[1].bind("<Button-1>", self.history)
        self.btns[2].bind("<Button-1>", self.setting)
        self.btns[3].bind("<Button-1>", self.help)

        self.btns[0].bind("<Motion>", lambda x: self.btns[0].config(image=self.btn_pics[0][1]))
        self.btns[1].bind("<Motion>", lambda x: self.btns[1].config(image=self.btn_pics[1][1]))
        self.btns[2].bind("<Motion>", lambda x: self.btns[2].config(image=self.btn_pics[2][1]))
        self.btns[3].bind("<Motion>", lambda x: self.btns[3].config(image=self.btn_pics[3][1]))

        self.btns[0].bind("<Leave>", lambda x: self.btns[0].config(image=self.btn_pics[0][0]))
        self.btns[1].bind("<Leave>", lambda x: self.btns[1].config(image=self.btn_pics[1][0]))
        self.btns[2].bind("<Leave>", lambda x: self.btns[2].config(image=self.btn_pics[2][0]))
        self.btns[3].bind("<Leave>", lambda x: self.btns[3].config(image=self.btn_pics[3][0]))


        # self.gif = giffing.ImageLabel(self.bg)
        # self.gif.place(x=10, y=450, width=155, height=300)

    def new(self,event):
        self.forms[1].show(self.forms)
        return

    def history(self,event):
        self.forms[2].show(self.forms)
        return

    def setting(self,event):
        self.forms[4].show(self.forms)
        return



    def help(self,event):
        print("how to use")
        return

    def show(self,forms):
        self.forms=forms
        self.bg.place(x=0,y=0,width=1024,height=768)
        # self.gif.load(self.path+"pics/gg.gif")
