from tkinter import  *
import sys, cv2
import numpy as np
from PIL import Image,ImageTk

import main_menu as mains
import new_screen as new
import history
import scanning, setting , analys
import report


def logoing():
    path=sys.path[0]
    bg=np.ones([768,1024,3],np.uint8)  #cv2.imread(path+'/pics/bg.png')
    bg=bg*255
    logo=cv2.imread(path+'/pics/logo.png')
    x1=int((1024-400)/2)
    y1=int((768-400)/2)

    logo_gray=cv2.cvtColor(logo,cv2.COLOR_BGR2GRAY)
    logo_gray_inv=cv2.bitwise_not(logo_gray)
    ret, logo_gray_inv = cv2.threshold(logo_gray_inv, 5, 255, cv2.THRESH_BINARY)
    logo_mask=logo_gray_inv
    logo_mask_inv=cv2.bitwise_not(logo_gray_inv)


    cropped_bg=bg[y1:y1+400,x1:x1+400]

    logo_out=cv2.bitwise_and(cropped_bg,cropped_bg,mask=logo_mask_inv)
    logo_in=cv2.bitwise_and(logo,logo,mask=logo_mask)

    logo=cv2.add(logo_out,logo_in)

    bg[y1:y1+400,x1:x1+400]=logo

    b,g,r=cv2.split(bg)
    bg=cv2.merge((r,g,b))

    img=Image.fromarray(bg)
    img=ImageTk.PhotoImage(img)

    # cv2.imwrite('/home/hossein/Desktop/logo.png',bg)
    return img





root=Tk()
root.geometry("1024x768+0+0")
root.overrideredirect(True)







menu=mains.meno(root)
new_s=new.form(root)
hs=history.form(root)
sc=scanning.form(root)
st=setting.form(root)
ana=analys.form(root)
rp=report.form(root)


forms=[menu,new_s,hs,sc,st,ana,rp]


menu.show(forms)


# inf=['HOS-TEST-2', '55884433', '1396-11-25']
# forms[6].show(forms,inf)





dm_pic=logoing()

dm=Label(root,image=dm_pic)
dm.place(x=0,y=0)
root.after(10,dm.place_forget)






btn=Button(root,text="exit",command=root.destroy)
btn.place(x=20,y=20)
root.mainloop()

