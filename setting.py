from tkinter import *
import sys
import os
import cv2
from tkinter import filedialog


class gride_selection:
    def __init__(self, master, par):
        self.path = sys.path[0]
        self.master = master
        self.par =par
        self.bg = Label(self.master)
        self.g_s = IntVar()
        self.g_c = IntVar()

        self.chk_1 = Checkbutton(self.bg, text="Small", variable=self.g_s, onvalue=0)
        self.chk_2 = Checkbutton(self.bg, text="Medium", variable=self.g_s, onvalue=1)
        self.chk_3 = Checkbutton(self.bg, text="Big", variable=self.g_s, onvalue=2)

        self.chk_4 = Checkbutton(self.bg, text="Big", variable=self.g_c, onvalue=0)
        self.chk_5 = Checkbutton(self.bg, text="Big", variable=self.g_c, onvalue=1)

        self.chk_1.place(x=30, y=100)
        self.chk_2.place(x=30, y=150)
        self.chk_3.place(x=30, y=200)

        self.chk_4.place(x=120, y=100)
        self.chk_5.place(x=120, y=150)

        self.finish_btn = Button(self.bg, text="Done", command=self.fin)
        self.finish_btn.place(x=(500 - 50) / 2, y=250)

    def show(self):
        self.bg.place(x=(1024 - 500) / 2, y=(768 - 300) / 2, width=500, height=300)

    def fin(self):
        res1 = self.g_s.get()
        res2 = self.g_c.get()

        self.par.add_gride(res1, res2)
        self.hide()

    def hide(self):
        self.bg.place_forget()


class form:
    def __init__(self, master):
        self.path = sys.path[0]

        self.bg_pic = PhotoImage(file=self.path + "/pics/setting/bg.png")
        self.bg = Label(master, image=self.bg_pic)

        self.bg.bind("<Button-1>", self.clicking)

        self.pics = [[PhotoImage(file=self.path + "/pics/setting/voice-off.png"),
                      PhotoImage(file=self.path + "/pics/setting/voice-on.png")],
                     [PhotoImage(file=self.path + "/pics/setting/bt-off.png"),
                      PhotoImage(file=self.path + "/pics/setting/bt-on.png")],
                     [PhotoImage(file=self.path + "/pics/setting/gride-on.png"),
                      PhotoImage(file=self.path + "/pics/setting/gride-on.png")],
                     [PhotoImage(file=self.path + "/pics/setting/wifi-off.png"),
                      PhotoImage(file=self.path + "/pics/setting/wifi-on.png")], ]

        self.lbs = [Label(self.bg, image=self.pics[0][0], bg="white"),
                    Label(self.bg, image=self.pics[1][0], bg="white"),
                    Label(self.bg, image=self.pics[2][0], bg="white"),
                    Label(self.bg, image=self.pics[3][0], bg="white")]

        self.lbs[0].place(x=217, y=155)
        self.lbs[1].place(x=631, y=155)
        self.lbs[2].place(x=215, y=410)
        self.lbs[3].place(x=631, y=410)
        self.lbs[0].bind("<Button-1>", lambda x: self.voice())
        self.lbs[1].bind("<Button-1>", lambda x: self.blt())
        self.lbs[2].bind("<Button-1>", lambda x: self.gride())
        self.lbs[3].bind("<Button-1>", lambda x: self.wifi())

        self.params = {"wifi": 0, "blt": 0, "gride": 0, "voice": 0}
        self.gride_form = gride_selection(self.bg, self)

    def clicking(self, event):
        x = event.x
        y = event.y
        if (x < 130) & (y < 130):
            self.hide()
        elif (x > 410) & (x < 610) & (y > 500) & (y < 590):
            self.com_inf(1)

    def show(self, forms):
        self.forms = forms
        self.bg.place(x=0, y=0, width=1024, height=768)
        self.load_setting()

    def hide(self):
        self.write_setting()
        self.bg.place_forget()

    def voice(self):
        self.params["voice"] = not self.params["voice"]
        self.btn_updater()
        return

    def blt(self):
        self.params["blt"] = not self.params["blt"]
        self.btn_updater()
        return

    def gride(self):
        # self.params["gride"]= not self.params["gride"]
        # self.btn_updater()
        self.gride_form.show()
        return

    def wifi(self):
        self.params["wifi"] = not self.params["wifi"]
        self.btn_updater()
        return

    def btn_updater(self):
        self.lbs[0].config(image=self.pics[0][self.params["voice"]])
        self.lbs[1].config(image=self.pics[1][self.params["blt"]])
        self.lbs[2].config(image=self.pics[2][self.params["gride"]])
        self.lbs[3].config(image=self.pics[3][self.params["wifi"]])

        return

    def add_gride(self, res1, res2):
        self.params["gride"] = str(res1)+":"+str(res2)

    def com_inf(self, fun):
        if fun == 1:
            try:
                path = "/media/hossein/" + self.chkusb()
                print(path)
                answer = filedialog.askopenfilename(parent=self.bg,
                                                    initialdir=path,
                                                    title="Please select a PNG file:",
                                                    filetypes=[('PNG', '.png')])
                self.logo_replace(answer)
            except:
                print("error")
        else:
            return

    def load_setting(self):
        f = open(self.path + "/setting.log", 'r')
        str = f.read()
        infs = str.split("!")
        self.params["gride"] = bool(int(infs[0]))
        self.params["voice"] = bool(int(infs[1]))
        self.params["blt"] = bool(int(infs[2]))
        self.params["wifi"] = bool(int(infs[3]))
        self.btn_updater()

        f.close()
        # print(self.voice_on)

    def write_setting(self):
        f = open(self.path + "/setting.log", "w")
        st = self.params["gride"] + "!"
        st = st + str(int(self.params["voice"])) + "!"
        st = st + str(int(self.params["blt"])) + "!"
        st = st + str(int(self.params["wifi"])) + "!"

        f.write(st)
        f.close()

    def logo_replace(self, path):
        img = cv2.imread(path)
        img = cv2.resize(img, (400, 400), interpolation=cv2.INTER_CUBIC)
        cv2.imwrite(self.path + '/pics/logo.png', img)

    def chkusb(self):
        usblist = os.listdir("/media/hossein")

        # print("only one:" + usblist[0])
        usbname = usblist[0]
        # print(usbname)

        return usbname

    #
    #
    #
    # def ask(self):
    #     self.fr = Frame(self.bg, width=300, height=200, bg="gray")
    #     self.lbq = Label(self.fr, text="Are You Sure?", bg="gray", font=("Arial", 30, 'bold'))
    #     self.lbq.place(x=30, y=80)
    #     self.btn0 = Button(self.fr, text="YES", command=self.yesi)
    #     self.btn1 = Button(self.fr, text="NO", command=self.noi)
    #     self.btn0.place(x=130, y=120)
    #     self.btn1.place(x=200, y=120)
    #
    #     self.fr.place(x=(1024 - 300) / 2, y=(768 - 200) / 2)
    #
    # def yesi(self):
    #     self.fr.place_forget()
    #     self.hide()
    #     return
    #
    # def noi(self):
    #     self.fr.place_forget()
    #     return
