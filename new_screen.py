from tkinter import *
import sys
from threading import Timer
import datetime as dt



class form:
    def __init__(self,master):
        self.path=sys.path[0]
        self.bg_pic=PhotoImage(file=self.path+"/pics/new_training/bg.png")
        self.bg=Label(master,image=self.bg_pic)
        self.bg.bind("<Button-1>",self.clicking)

        self.chkp=[PhotoImage(file=self.path+'/pics/new_training/check-off.png'),PhotoImage(file=self.path+'/pics/new_training/check-on.png')]

        self.sexlb=[Label(self.bg,image=self.chkp[1],bg='white'),
                  Label(self.bg, image=self.chkp[0],bg='white')]
        self.sexlb[0].place(x=430, y=315)
        self.sexlb[1].place(x=690, y=315)

        self.sex="M"
        self.sexlb[0].bind("<Button-1>", self.sex_male)
        self.sexlb[1].bind("<Button-1>", self.sex_female)







        self.informations=[StringVar(),StringVar(),StringVar()]
        self.informations[2].set("YYYY-MM-DD")

        self.lbs=[Label(self.bg, textvariable=self.informations[0], bg="white",width=20, font=("Arial", 30, 'bold'), fg="darkblue",anchor=W),
                  Label(self.bg, textvariable=self.informations[1], bg="white",width=20, font=("Arial", 30, 'bold'), fg="darkblue",anchor=W),
                  Label(self.bg, textvariable=self.informations[2], bg="white",width=20, font=('Arial', 35, 'bold'), fg="darkblue",anchor=W)]

        self.lbs[0].place(x=310, y=165)
        self.lbs[1].place(x=220, y=245)
        self.lbs[2].place(x=300,y=395)

        self.lbs[0].bind("<Button-1>", self.fun0)
        self.lbs[1].bind("<Button-1>", self.fun1)
        self.lbs[2].bind("<Button-1>", self.fun2)

        self.tab=0

        self.releasing()
        self.cert=["" for i in range(4)]


    def sex_male(self, event):
        self.sex = "M"
        self.sexlb[0].configure(image=self.chkp[1])
        self.sexlb[1].configure(image=self.chkp[0])

    def sex_female(self, event):
        self.sex = "F"
        self.sexlb[1].configure(image=self.chkp[1])
        self.sexlb[0].configure(image=self.chkp[0])


    def clicking(self,event):
        x=event.x
        y=event.y
        w=62
        h=62

        if (x>90)&(x<(90+w))&(y>546)&(y<(546+h)):
            self.adding("Q")
        elif (x>(90+w))&(x<(90+2*w))&(y>(546))&(y<(546+h)):
            self.adding("W")
        elif (x>(90+2*w))&(x<(90+3*w))&(y>(546))&(y<(546+h)):
            self.adding("E")
        elif (x>(90+3*w))&(x<(90+4*w))&(y>(546))&(y<(546+h)):
            self.adding("R")
        elif (x>(90+4*w))&(x<(90+5*w))&(y>(546))&(y<(546+h)):
            self.adding("T")
        elif (x>(90))&(x<(90+w))&(y>(546+h))&(y<(546+2*h)):
            self.adding("A")
        elif (x>(90+w))&(x<(90+2*w))&(y>(546+h))&(y<(546+2*h)):
            self.adding("S")
        elif (x>(90+2*w))&(x<(90+3*w))&(y>(546+h))&(y<(546+2*h)):
            self.adding("D")
        elif (x>(90+3*w))&(x<(90+4*w))&(y>(546+h))&(y<(546+2*h)):
            self.adding("F")
        elif (x>(90+4*w))&(x<(90+5*w))&(y>(546+h))&(y<(546+2*h)):
            self.adding("G")
        elif (x>(90))&(x<(90+w))&(y>(546+2*h))&(y<(546+3*h)):
            self.adding("Z")
        elif (x>(90+w))&(x<(90+2*w))&(y>(546+2*h))&(y<(546+3*h)):
            self.adding("X")
        elif (x>(90+2*w))&(x<(90+3*w))&(y>(546+2*h))&(y<(546+3*h)):
            self.adding("C")
        elif (x>(90+3*w))&(x<(90+4*w))&(y>(546+2*h))&(y<(546+3*h)):
            self.adding("V")
        elif (x>(90+4*w))&(x<(90+5*w))&(y>(546+2*h))&(y<(546+3*h)):
            self.adding("B")


        elif (x>(418))&(x<(418+w))&(y>(546))&(y<(546+h)):
            self.adding("Y")
        elif (x>(418+w))&(x<(418+2*w))&(y>(546))&(y<(546+h)):
            self.adding("U")
        elif (x>(418+2*w))&(x<(418+3*w))&(y>(546))&(y<(546+h)):
            self.adding("I")
        elif (x>(418+3*w))&(x<(418+4*w))&(y>(546))&(y<(546+h)):
            self.adding("P")
        elif (x > (418)) & (x < (418 + w)) & (y > (546 + h)) & (y < (546 + 2 * h)):
            self.adding("H")
        elif (x > (418 + w)) & (x < (418 + 2 * w)) & (y > (546 + h)) & (y < (546 + 2 * h)):
            self.adding("J")
        elif (x > (418 + 2 * w)) & (x < (418 + 3 * w)) & (y > (546 + h)) & (y < (546 + 2 * h)):
            self.adding("K")
        elif (x > (418 + 3 * w)) & (x < (418 + 4 * w)) & (y > (546 + h)) & (y < (546 + 2 * h)):
            self.adding("L")
        elif (x>(418))&(x<(418+w))&(y>(546+2*h))&(y<(546+3*h)):
            self.adding("N")
        elif (x>(418+w))&(x<(418+2*w))&(y>(546+2*h))&(y<(546+3*h)):
            self.adding("M")
        elif (x>(418+2*w))&(x<(418+3*w))&(y>(546+2*h))&(y<(546+3*h)):
            self.adding("O")
        elif (x>(418+3*w))&(x<(418+4*w))&(y>(546+2*h))&(y<(546+3*h)):
            self.adding("-")


        elif (x>(684))&(x<(684+w))&(y>(546))&(y<(546+h)):
            self.adding("7")
        elif (x>(684+w))&(x<(684+2*w))&(y>(546))&(y<(546+h)):
            self.adding("8")
        elif (x>(684+2*w))&(x<(684+3*w))&(y>(546))&(y<(546+h)):
            self.adding("9")
        elif (x>(684+3*w))&(x<(684+4*w))&(y>(546))&(y<(546+h)):
            self.adding("DEL")
        elif (x > (684)) & (x < (684 + w)) & (y > (546 + h)) & (y < (546 + 2 * h)):
            self.adding("4")
        elif (x > (684 + w)) & (x < (684 + 2 * w)) & (y > (546 + h)) & (y < (546 + 2 * h)):
            self.adding("5")
        elif (x > (684 + 2 * w)) & (x < (684 + 3 * w)) & (y > (546 + h)) & (y < (546 + 2 * h)):
            self.adding("6")
        elif (x > (684 + 3 * w)) & (x < (684 + 4 * w)) & (y > (546 + h)) & (y < (546 + 2 * h)):
            self.adding("DEL")
        elif (x>(684))&(x<(684+w))&(y>(546+2*h))&(y<(546+3*h)):
            self.adding("1")
        elif (x>(684+w))&(x<(684+2*w))&(y>(546+2*h))&(y<(546+3*h)):
            self.adding("2")
        elif (x>(684+2*w))&(x<(684+3*w))&(y>(546+2*h))&(y<(546+3*h)):
            self.adding("3")
        elif (x>(684+3*w))&(x<(684+4*w))&(y>(546+2*h))&(y<(546+3*h)):
            self.adding("0")



        elif (x>(660+0*w))&(x<(660+1*w))&(y>(701))&(y<(701+h)):
            self.remove()


        elif (x<160)&(y<160):
            self.hide()
        elif (x > (1024-160)) & (y < 160):
            self.finishing()

        return x,y

    def show(self,forms):
        self.forms=forms
        self.bg.place(x=0,y=0,width=1024,height=768)
        for i in range(len(self.informations)):self.informations[i].set("")
        self.informations[0].set("_")
        self.informations[2].set("YYYY-MM-DD")
        
        self.pointer_splasher(True)

    def hide(self):
        for i in range(3):self.informations[i].set("")
        self.tab=0
        self.informations[0].set("_")
        self.informations[2].set("YYYY-MM-DD")
        self.timer.cancel()
        self.bg.place_forget()

    def finishing(self):
        st=self.informations[2].get()
        st=st.split("_")
        st=st[0]
        if (len(st)!=10) or (not self.chck_date(st)):
            return
        for i in range(len(self.informations)):
            sst=self.informations[i].get()
            st=sst.split("_")
            self.cert[i]=st[0]
        self.cert[3]=self.sex
        self.forms[3].show(self.forms,self.cert)
        self.hide()

    def chck_date(self,st):
        s=st.split("-")
        if (s[0].find("Y")==-1 & s[1].find("M")==-1 & s[2].find("D")==-1) : return True
        return False

    def adding(self,st):
        if self.tab==2:
            if(st in ["1","2","3","4","5","6","7","8","9","0","-"]):
                strr = self.informations[self.tab].get()
                strr = strr.split("_")
                strr=strr[0].split("-")

                if (strr[0].find("Y")!=-1):
                    q=strr[0].find("Y")
                    s=strr[0].split("Y")
                    s=s[0]+st
                    strr[0]=s+"Y"*(3-q)
                elif (strr[1].find("M")!=-1):
                    q = strr[1].find("M")
                    s = strr[1].split("M")
                    s = s[0] + st
                    strr[1] = s + "M" * (1 - q)
                elif (strr[2].find("D")!=-1):
                    q = strr[2].find("D")
                    s = strr[2].split("D")
                    s = s[0] + st
                    strr[2] = s + "D" * (1 - q)

                strt=strr[0]+"-"+strr[1]+"-"+strr[2]
                self.informations[self.tab].set(strt)

            elif st == "DEL":
                strr = self.informations[self.tab].get()
                strr=strr.split("-")

                if(strr[2].find("D")==-1 or strr[2].find("D")==1):
                    q=strr[2].find("D")
                    if q==-1:strr[2]=strr[2][0]+"D"
                    if q == 1: strr[2] ="DD"
                elif (strr[1].find("M")==-1 or strr[1].find("M")==1):
                    q = strr[1].find("M")
                    if q == -1: strr[1] = strr[1][0] + "M"
                    if q == 1: strr[1] = "MM"
                elif (strr[0].find("Y") == -1 or strr[0].find("Y")>= 1):
                    q = strr[0].find("Y")
                    print(q)
                    if q == -1: strr[0] = strr[0][0:3] + "Y"
                    if q >= 1: strr[0]=strr[0][0:(q-1)]+"Y"*(5-q)

                strt=strr[0]+"-"+strr[1]+"-"+strr[2]
                self.informations[self.tab].set(strt)
                return

            return



        if st=="DEL":
            strr = self.informations[self.tab].get()
            strr=strr.split("_")
            strr=strr[0][0:(len(strr[0])-1)]
            st=strr+"_"
            self.informations[self.tab].set(st)
            return
        strr=self.informations[self.tab].get()
        strr=strr.split("_")
        strt=strr[0]+st+"_"
        self.informations[self.tab].set(strt)
        return

    def pointer_splasher(self,s):
        if s :
            stt=self.informations[self.tab].get()
            stt=stt.split("_")
            self.informations[self.tab].set(stt[0])
        else:
            stt = self.informations[self.tab].get()
            stt = stt.split("_")
            stt = stt[0]+"_"
            self.informations[self.tab].set(stt)

        s= not s

        self.timer=Timer(0.7,self.pointer_splasher,args=[s])
        self.timer.start()

        return

    def fun0(self,event):
        self.tab=0
        self.releasing()
    def fun1(self,event):
        self.tab=1
        self.releasing()
    def fun2(self,event):
        self.tab=2
        self.releasing()

    def releasing(self):
        for i in range(3):
            ss=self.informations[i].get()
            ssq=ss.split("_")
            ss=ssq[0]
            self.informations[i].set(ss)
        ss=self.informations[self.tab].get()
        ss=ss+"_"
        self.informations[self.tab].set(ss)

