from tkinter import *
import sys
import os, os.path
import search_frames as Frames
from Keyboard import keyboard as KEYBOARD
import tkinter.ttk as ttk


class form:
    def __init__(self,master):
        self.path=sys.path[0]

        self.bg_pic=PhotoImage(file=self.path+"/pics/history/bg.png")
        self.search_bg_pic=PhotoImage(file=self.path+"/pics/history/bg_search.png")
        self.bg=Label(master,image=self.bg_pic)


        self.btn_pics=[[PhotoImage(file=self.path+"/pics/history/home.png"),
                        PhotoImage(file=self.path + "/pics/history/home-click.png")],
                       [PhotoImage(file=self.path + "/pics/history/search.png"),
                        PhotoImage(file=self.path + "/pics/history/search-click.png")],
                       [PhotoImage(file=self.path + "/pics/history/load.png"),
                        PhotoImage(file=self.path + "/pics/history/load-click.png")],
                       [PhotoImage(file=self.path + "/pics/history/delete.png"),
                        PhotoImage(file=self.path + "/pics/history/delete-click.png")]]

        self.btns=[Label(self.bg, image=self.btn_pics[0][0]),
                   Label(self.bg, image=self.btn_pics[1][0]),
                   Label(self.bg, image=self.btn_pics[2][0]),
                   Label(self.bg, image=self.btn_pics[3][0])]
        self.btns[0].place(x=58, y=90)
        self.btns[1].place(x=58, y=255)
        self.btns[2].place(x=58, y=425)
        self.btns[3].place(x=58, y=590)

        self.btns[0].bind("<Motion>",lambda x: self.btns[0].config(image=self.btn_pics[0][1]))
        self.btns[0].bind("<Leave>", lambda x: self.btns[0].config(image=self.btn_pics[0][0]))
        self.btns[1].bind("<Motion>", lambda x: self.btns[1].config(image=self.btn_pics[1][1]))
        self.btns[1].bind("<Leave>", lambda x: self.btns[1].config(image=self.btn_pics[1][0]))
        self.btns[2].bind("<Motion>", lambda x: self.btns[2].config(image=self.btn_pics[2][1]))
        self.btns[2].bind("<Leave>", lambda x: self.btns[2].config(image=self.btn_pics[2][0]))
        self.btns[3].bind("<Motion>", lambda x: self.btns[3].config(image=self.btn_pics[3][1]))
        self.btns[3].bind("<Leave>", lambda x: self.btns[3].config(image=self.btn_pics[3][0]))
        self.btns[0].bind("<1>", lambda x: self.hide())
        self.btns[1].bind("<1>", lambda x: self.search())
        self.btns[2].bind("<1>", lambda x: self.analys())
        self.btns[3].bind("<1>", lambda x: self.list_del())

        self.search_en=False

        self.search_btn_pics = [PhotoImage(file=self.path + "/pics/history/btns/simple_Date.png"),
                PhotoImage(file=self.path + "/pics/history/btns/Date_Date.png"),
                PhotoImage(file=self.path + "/pics/history/btns/simple_name.png"),
                PhotoImage(file=self.path + "/pics/history/btns/Date_name.png"),
                PhotoImage(file=self.path + "/pics/history/btns/simple_id.png"),
                PhotoImage(file=self.path + "/pics/history/btns/Date_id.png"),
                PhotoImage(file=self.path + "/pics/history/btns/id_id.png"),
                PhotoImage(file=self.path + "/pics/history/btns/name_name.png")]

        self.search_lbs = [Label(self.bg, image=self.search_btn_pics[0]),
               Label(self.bg, image=self.search_btn_pics[2]),
               Label(self.bg, image=self.search_btn_pics[4])]

        self.search_lbs[0].place(x=393, y=38, width=198, height=58)
        self.search_lbs[1].place(x=198+393, y=38, width=198, height=58)
        self.search_lbs[2].place(x=396+393, y=38, width=198, height=58)

        self.search_lbs[0].bind("<Button-1>", lambda x: self.change(1))
        self.search_lbs[1].bind("<Button-1>", lambda x: self.change(2))
        self.search_lbs[2].bind("<Button-1>", lambda x: self.change(3))

        #self.load()
        self.Data=[]
        self.sample_Data=self.Data


        self.Main_fr=Frames.main_plan(self.bg)
        self.Data_fr=Frames.Date_plan(self.bg)
        self.Name_fr=Frames.Name_plan(self.bg)
        self.Id_fr=Frames.Id_plan(self.bg)
        self.Main_fr.load()
        self.Main_fr.list_loader(self.sample_Data)
        self.top_frame=0

        self.key_board = KEYBOARD(self.bg)
        self.key_board.load(bg='#caced6')

    def show(self,forms):
        self.forms=forms
        self.bg.place(x=0,y=0,width=1024,height=768)
        self.load()
        self.Main_fr.list_loader(self.Data)

    def hide(self):
        # for i in self.list.get_children():
        #     self.list.delete(i)
        self.Data=[]
        self.bg.place_forget()

    def search(self):
        self.search_en= not self.search_en
        if self.search_en:
            self.btns[1].config(image=self.btn_pics[1][1])
            self.bg.config(image=self.search_bg_pic)
            self.Data_fr.load(self.key_board)
            self.Data_fr.list_loader(self.Data)
            self.search_lbs[0].config(image=self.search_btn_pics[1])
            self.search_lbs[1].config(image=self.search_btn_pics[3])
            self.search_lbs[2].config(image=self.search_btn_pics[5])
            self.top_frame = 1

        else:
            self.btns[1].config(image=self.btn_pics[1][0])
            self.bg.config(image=self.bg_pic)
            self.Data_fr.unloader()
            self.Name_fr.unloader()
            self.Id_fr.unloader()
            self.Main_fr.load()
            self.Main_fr.list_loader(self.Data)
            self.search_lbs[0].config(image=self.search_btn_pics[0])
            self.search_lbs[1].config(image=self.search_btn_pics[2])
            self.search_lbs[2].config(image=self.search_btn_pics[4])
            self.top_frame = 0


        return

    def change(self,i):
        if self.search_en:

            if i == 1:
                self.search_lbs[0].config(image=self.search_btn_pics[1])
                self.search_lbs[1].config(image=self.search_btn_pics[3])
                self.search_lbs[2].config(image=self.search_btn_pics[5])
                self.Name_fr.unloader()
                self.Id_fr.unloader()
                self.Data_fr.load(self.key_board)
                self.Data_fr.list_loader(self.Data)
                self.top_frame = 1

            elif i == 2:
                self.search_lbs[0].config(image=self.search_btn_pics[0])
                self.search_lbs[1].config(image=self.search_btn_pics[7])
                self.search_lbs[2].config(image=self.search_btn_pics[5])
                self.Data_fr.unloader()
                self.Id_fr.unloader()
                self.Name_fr.load(self.key_board)
                self.Name_fr.list_loader(self.Data)
                self.top_frame = 2

            elif i == 3:
                self.search_lbs[0].config(image=self.search_btn_pics[0])
                self.search_lbs[1].config(image=self.search_btn_pics[3])
                self.search_lbs[2].config(image=self.search_btn_pics[6])
                self.Data_fr.unloader()
                self.Name_fr.unloader()
                self.Id_fr.load(self.key_board)
                self.Id_fr.list_loader(self.kData)
                self.top_frame = 3

        return

    def load(self):
        DIR = self.path+"/Histories"
        Date_dirs = [name for name in os.listdir(DIR) if not (os.path.isfile(os.path.join(DIR, name)))]
        self.Data = []
        if len(Date_dirs)==0:

            return

        # print(Date_dirs)
        for i in Date_dirs:
            dir_2=self.path+"/Histories/"+i
            peoples=[name for name in os.listdir(dir_2) if not (os.path.isfile(os.path.join(dir_2, name)))]

            for j in peoples:
                
                dd=j.split(":")
                name=dd[0]
                id_r=dd[1]
                self.Data.append([i,name,id_r])



            pass
        # print(self.Data)

    def clicking(self,event):
        x=event.x
        y=event.y


        if (x>240)&(x<350)&(y>220)&(y<325):
            self.analys()
        elif (x>240)&(x<350)&(y>350)&(y<455):
            print("edit")
        elif (x > 240) & (x < 350) & (y > 480) & (y < 585):
            self.list_del()


        elif (x<120)&(y<120):
            self.ask()

    def analys(self):
        if self.top_frame==0:
            q=self.Main_fr.tree.focus()
            f = self.Main_fr.tree.item(q, "value")
        elif self.top_frame == 1:
            q = self.Data_fr.tree.focus()
            f = self.Data_fr.tree.item(q, "value")
        elif self.top_frame==2:
            q = self.Name_fr.tree.focus()
            f = self.Name_fr.tree.item(q, "value")
        elif self.top_frame==3:
            q = self.Id_fr.tree.focus()
            f = self.Id_fr.tree.item(q, "value")
        informs=[f[1],f[2],f[0]]
        self.forms[5].show(self.forms, informs)
        print(f)
        return

    def list_del(self):
        if self.top_frame==0:
            q=self.Main_fr.tree.focus()
            f = self.Main_fr.tree.item(q, "value")
        elif self.top_frame == 1:
            q = self.Data_fr.tree.focus()
            f = self.Data_fr.tree.item(q, "value")
        elif self.top_frame==2:
            q = self.Name_fr.tree.focus()
            f = self.Name_fr.tree.item(q, "value")
        elif self.top_frame==3:
            q = self.Id_fr.tree.focus()
            f = self.Id_fr.tree.item(q, "value")



        try:
            # par = self.list.parent(q)
            # Dt=self.list.item(par,"text")
            # fs=f[3].split("/")
            # file_name=self.list.item(q,"text")+":"+f[0]+":"+fs[0]+":"+fs[1]+":"+f[2]+":"+f[1]+":"+f[4]
            path=self.path+"/Histories/"+f[0]+"/"+f[1]+":"+f[2]
            cmd="rm -r "+path
            os.system(cmd)
        except:
            file_name=f[0]
            path = self.path + "/Histories/" + file_name
            cmd = "rm -r " + path
            os.system(cmd)

        if self.top_frame==0:
            self.Main_fr.tree.delete(q)
        elif self.top_frame == 1:
            self.Data_fr.tree.delete(q)
        elif self.top_frame==2:
            self.Name_fr.tree.delete(q)
        elif self.top_frame==3:
            self.Id_fr.tree.delete(q)







    def ask(self):
        self.fr = Frame(self.bg, width=300, height=200, bg="gray")
        self.lbq = Label(self.fr, text="Are You Sure?", bg="gray", font=("Arial", 30, 'bold'))
        self.lbq.place(x=30, y=80)
        self.btn0 = Button(self.fr, text="YES", command=self.yesi)
        self.btn1 = Button(self.fr, text="NO", command=self.noi)
        self.btn0.place(x=130, y=120)
        self.btn1.place(x=200, y=120)

        self.fr.place(x=(1024 - 300) / 2, y=(768 - 200) / 2)

    def yesi(self):
        self.fr.place_forget()
        self.hide()
        return

    def noi(self):
        self.fr.place_forget()
        return
