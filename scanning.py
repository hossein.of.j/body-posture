

from tkinter import *

import sys
import os
from PIL import ImageTk
import opencv_cammera as op

class form:
    def __init__(self,master):
        self.path=sys.path[0]
        self.bg_pic=PhotoImage(file=self.path+"/pics/scanning/bg.png")
        self.bg=Label(master,image=self.bg_pic)
        self.bg.bind("<Button-1>", self.clicking)

        self.sc=op.Screen_label(self.bg)

        self.demo_pics=[PhotoImage(file=self.path + "/pics/scanning/front-n.png"),
                        PhotoImage(file=self.path + "/pics/scanning/side-n.png"),
                        PhotoImage(file=self.path + "/pics/scanning/back-n.png")]



        self.demo = Label(self.bg,image=self.demo_pics[0], bg="white", font=('Arial',36,'bold'))

        self.demo.place(x=760, y=160)



        self.lvls=[[PhotoImage(file=self.path + "/pics/scanning/FRONT.png"),
                    PhotoImage(file=self.path + "/pics/scanning/FRONT-CLICK.png")],
                   [PhotoImage(file=self.path + "/pics/scanning/SIDE.png"),
                    PhotoImage(file=self.path + "/pics/scanning/SIDE-CLICK.png")],
                   [PhotoImage(file=self.path + "/pics/scanning/BACK_v.png"),
                    PhotoImage(file=self.path + "/pics/scanning/BACK-CLICK.png")]]

        self.level_lbs=[Label(self.bg,image=self.lvls[0][0],bg='white'),
                        Label(self.bg,image=self.lvls[1][0],bg='white'),
                        Label(self.bg,image=self.lvls[2][0],bg='white')]

        self.level_lbs[0].place(x=38,y=195)
        self.level_lbs[1].place(x=38, y=335)
        self.level_lbs[2].place(x=38, y=470)

        self.level_lbs[0].bind("<Button-1>", lambda x: self.leveling(1))
        self.level_lbs[1].bind("<Button-1>", lambda x: self.leveling(2))
        self.level_lbs[2].bind("<Button-1>", lambda x: self.leveling(3))


        self.btn_pics=[[PhotoImage(file=self.path + "/pics/scanning/CAMERA.png"),PhotoImage(file=self.path + "/pics/scanning/CAMERA-CLICK.png")],
                       PhotoImage(file=self.path + "/pics/scanning/NEXT.png"),PhotoImage(file=self.path + "/pics/scanning/BACK.png")]

        self.cap=Label(self.bg,image=self.btn_pics[0][0],bg="white")
        self.cap.place(x=427 , y=635)
        self.cap.bind("<Button-1>",self.cp)
        self.cap.bind("<Motion>",lambda x:self.cap.config(image=self.btn_pics[0][1]))
        self.cap.bind("<Leave>", lambda x:self.cap.config(image=self.btn_pics[0][0]))



        self.next_btn=Label(self.bg,image=self.btn_pics[1],bg="white")
        self.next_btn.bind("<Button-1>",self.analys)
        # self.next_btn.place(x=875, y=38, width=115, height=115)


        self.stage=0
        self.s_pics=["","",""]
        self.paused=False

        self.sc.load()

        # self.tst = Label(self.bg, bg="green").place(x=450, y=640 ,width=130, height=130)

    def show(self,forms,informations):
        self.forms=forms
        self.informs=informations
        self.bg.place(x=0,y=0,width=1024,height=768)
        self.s_pics=["","",""]
        self.sc.place(x=336, y=123, width=351, height=465)
        self.sc.start()
        self.leveling(1)



    def hide(self):
        #self.sc.close()
        self.next_btn.place_forget()
        self.bg.place_forget()


    def cp(self,event):
        if self.paused:
            self.sc.start()
            self.paused = False
            return
        if self.stage <= 3:
            self.s_pics[self.stage - 1] = self.sc.capture()
            self.leveling(self.stage + 1)


        return


    def clicking(self,event):
        x=event.x
        y=event.y


        if (x>915)&(x<1015)&(y>540)&(y<640):
            self.saver()
            self.ask()

        elif (x>450)&(x<(450+130))&(y>640)&(y<770):
            self.leveling(self.stage-1)

        elif (x<120)&(y<120):
            self.ask()

    def leveling(self,i):
        if (i==1):
            self.demo.config(image=self.demo_pics[0])
            self.level_lbs[0].config(image=self.lvls[0][0])
            self.level_lbs[1].config(image=self.lvls[1][1])
            self.level_lbs[2].config(image=self.lvls[2][1])
            self.stage = 1
            if self.s_pics[0]!= "":
                self.sc.stop()
                self.sc.set_pic(self.s_pics[0])
                self.paused=True

        elif (i==2):
            # self.demo_txt.set("BACK")
            self.demo.config(image=self.demo_pics[1])
            self.level_lbs[0].config(image=self.lvls[0][1])
            self.level_lbs[1].config(image=self.lvls[1][0])
            self.level_lbs[2].config(image=self.lvls[2][1])
            self.stage = 2
            if self.s_pics[1]!= "":
                self.sc.stop()
                self.sc.set_pic(self.s_pics[1])
                self.paused = True

        elif (i==3):
            # self.demo_txt.set("SIDE")
            self.demo.config(image=self.demo_pics[2])
            self.level_lbs[0].config(image=self.lvls[0][1])
            self.level_lbs[1].config(image=self.lvls[1][1])
            self.level_lbs[2].config(image=self.lvls[2][0])
            self.stage = 3
            if self.s_pics[2]!= "":
                self.sc.stop()
                self.ppi=ImageTk.PhotoImage(self.s_pics[2])
                self.sc.config(image=self.ppi)
                self.paused = True
        elif(i==4):
            self.next_btn.place(x=875, y=38, width=115, height=115)


    def analys(self,event):
        self.saver()
        self.forms[5].show(self.forms,self.informs)
        self.sc.stop()
        self.hide()


    def ask(self):
        self.fr = Frame(self.bg, width=300, height=200, bg="gray")
        self.lbq = Label(self.fr, text="Are You Sure?", bg="gray", font=("Arial", 30, 'bold'))
        self.lbq.place(x=30, y=80)
        self.btn0 = Button(self.fr, text="YES", command=self.yesi)
        self.btn1 = Button(self.fr, text="NO", command=self.noi)
        self.btn0.place(x=130, y=120)
        self.btn1.place(x=200, y=120)

        self.fr.place(x=(1024 - 300) / 2, y=(768 - 200) / 2)

    def yesi(self):
        self.sc.stop()
        self.fr.place_forget()
        self.hide()
        return

    def noi(self):
        self.fr.place_forget()
        return





    def saver(self):
        fldr_name = self.informs[0] + ":" + self.informs[1] #+ ":" + self.informs[3]

        fldr = "mkdir -p " + self.path + "/Histories/" + self.informs[2] + "/" + fldr_name
        self.fpath = self.path + "/Histories/" + self.informs[2] + "/" + fldr_name
        os.system(fldr)
        for i in range(3):
            if self.s_pics[i] != "":
                self.s_pics[i].save(self.fpath + "/no_" + str(i) + ".png")

        print("saved")


