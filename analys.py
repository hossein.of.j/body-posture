from tkinter import *
import numpy as np
import sys
import cv2
from PIL import ImageTk, Image
import os
from time import sleep
from threading import Timer


class form:
    def __init__(self, master):

        self.path = sys.path[0]
        self.bg_pic = PhotoImage(file=self.path + "/pics/analys/bg.png")
        self.bg = Label(master, image=self.bg_pic)

        self.pics = ["", "", ""]
        self.checks = ["", "", ""]
        self.lines = ["", "", ""]

        self.screen = Label(self.bg, bg="yellow")
        self.screen.place(x=341, y=108, width=443, height=589)

        self.screen.bind("<Motion>", self.moving)
        self.screen.bind("<ButtonRelease>", self.clicked)

        self.nxt_pic = [PhotoImage(file=self.path + "/pics/analys/next.png"),
                        PhotoImage(file=self.path + "/pics/analys/next_clc.png")]

        self.back_pic = [PhotoImage(file=self.path + "/pics/analys/back_arrow.png"),
                         PhotoImage(file=self.path + "/pics/analys/back_arrow_clc.png")]

        self.bckwrd_pic = PhotoImage(file=self.path + "/pics/analys/backward.png")

        self.Marker_lb = [cv2.imread(self.path + "/pics/analys/marker.png"),
                          cv2.imread(self.path + "/pics/analys/marker-click.png")]
        self.marker_Loader()

        self.btns = [Label(self.bg, image=self.nxt_pic[0], bg="white"),
                     Label(self.bg, image=self.bckwrd_pic, bg="white"),
                     Label(self.bg, image=self.back_pic[0], bg="white")]
        self.btns[0].place(x=876, y=39)
        self.btns[1].place(x=795, y=600)
        self.btns[2].place(x=38, y=39)

        self.btns[0].bind("<Button-1>", self.next)
        self.btns[0].bind("<Motion>", lambda x: self.btns[0].config(image=self.nxt_pic[1]))
        self.btns[0].bind("<Leave>", lambda x: self.btns[0].config(image=self.nxt_pic[0]))

        self.btns[1].bind("<Button-1>", lambda x: self.back())

        self.btns[0].bind("<Button-1>", self.next)
        self.btns[2].bind("<Motion>", lambda x: self.btns[2].config(image=self.back_pic[1]))
        self.btns[2].bind("<Leave>", lambda x: self.btns[2].config(image=self.back_pic[0]))
        self.btns[2].bind("<1>", lambda x: self.hide())
        self.demo_pics = [cv2.imread(self.path + "/pics/analys/front-n.png", 1),
                          cv2.imread(self.path + "/pics/analys/side-n.png", 1),
                          cv2.imread(self.path + "/pics/analys/back-n.png", 1)]
        self.marker_pos = [
            [(64, 26), (78, 26), (35, 67), (106, 67), (71, 97), (71, 143), (45, 158), (92, 158), (49, 259), (49, 271),
             (49, 295), (46, 352),
             (92, 259), (92, 271), (92, 295), (94, 352)],

            [(66, 30), (62, 65), (47, 61), (40, 96), (47, 126), (47, 145), (40, 160), (80, 167), (62, 184), (73, 256),
             (73, 339), (109, 353)],

            [(69, 55), (35, 66), (105, 66), (55, 98), (69, 98), (83, 98), (69, 130), (58, 157), (81, 157), (50, 330),
             (51, 347), (87, 330),
             (88, 347)]]

        self.marker_names = [
            ["Right eye", "Left eye", "Right acromion", "Left acromion", "Ribcage", "Bellybottom", "Right asis",
             "Left asis",
             "Right patella", "R-Tibia-tobersity", "Right tibia", "R-2th metatarsal ", "Left knee", "L-Tibia-tobersity",
             "Left tibia",
             "L-2th metatarsal", "Finish"],

            ["Mastuid", "Acromion", "C7", "T8", "T12", "L5", "S1", "asis", "Greater trochanter",
             "Lateral knee", "talus", "5th metetarsal", "Finish"],

            ["C7", "L-acromioclavicular", "R-acromioclavicular", "L-subscapula",
             "T8", "R-subscapula",
             "T12", "Left psis", "Right psis", "L-Achilles tendon",
             "L-calcanaus",
             "R-Achilles‬‬ ‫‪tendon‬‬", "R-calcanaus‬‬", "Finish"]
        ]
        self.marker_nm = StringVar()
        self.marker_nm.set(self.marker_names[0][0])
        self.Marker_name = Label(self.bg, textvariable=self.marker_nm, bg="white", fg="darkblue",
                                 font=("Arial", 20, 'bold'), anchor=CENTER).place(x=20, y=300, width=300, height=50)

        self.demo_pics_tk = ["", "", ""]
        for i in range(3):
            cv2.circle(self.demo_pics[i], self.marker_pos[i][0], 4, (200, 100, 0), -1)
            b, g, r = cv2.split(self.demo_pics[i])
            img = cv2.merge((r, g, b))
            img = Image.fromarray(img)
            self.demo_pics_tk[i] = ImageTk.PhotoImage(img)

        self.demoes = [Label(self.bg, image=self.demo_pics_tk[0], bg='white'),
                       Label(self.bg, image=self.demo_pics_tk[1], bg='white'),
                       Label(self.bg, image=self.demo_pics_tk[2], bg='white')]

        self.demoes[0].place(x=845, y=190)
        # self.demoes[1].place(x=845, y=190)
        # self.demoes[2].place(x=845, y=190)

        self.txt_pics = [PhotoImage(file=self.path + "/pics/analys/front_txt.png"),
                         PhotoImage(file=self.path + "/pics/analys/side_txt.png"),
                         PhotoImage(file=self.path + "/pics/analys/back_txt.png")]

        self.txt_lbs = [Label(self.bg, image=self.txt_pics[0], bg='white'),
                        Label(self.bg, image=self.txt_pics[1], bg='white'),
                        Label(self.bg, image=self.txt_pics[2], bg='white')]
        self.txt_lbs[0].place(x=420, y=714)

        self.demo_checke_level = [0, 0, 0]

        self.pointer_pics = []

        self.check_points = [[], [], []]

        self.zoom_pic = self.zoomer_init()
        self.zoom = Label(self.bg, image=self.zoom_pic, bg="white")
        self.zoom.place(x=5, y=365)
        self.autodet = 0
        self.level = 0

        self.grid_x = 30
        self.grid_y = 30

        self.touch_margin = (40, 45)

        self.prompt = Label(self.bg, text="noting", bg="orange", font=('Arial', 35, 'bold'))

        #
        # self.reset_lb=Label(self.bg,text="RESET",font=("Arial",30,'bold'),bg='black',fg='red')
        # self.reset_lb.place(x=800,y=40)
        # self.reset_lb.bind("<Button-1>",self.reset)
        # self.tst=Label(self.bg,bg="blue").place(x=24,y=348,width=70,height=70)
        return

    def show(self, forms, inf):
        self.forms = forms
        self.informs = inf
        self.bg.place(x=0, y=0, width=1024, height=768)
        self.load_setting()
        # print(inf)

        self.loading()
        self.leveling(1)
        self.marker_spladsher(True)

    def marker_Loader(self):

        self.M_shp = (78, 67)
        self.Marker_lb[0] = cv2.resize(self.Marker_lb[0], (self.M_shp[1], self.M_shp[0]))
        self.Marker_lb[1] = cv2.resize(self.Marker_lb[1], (self.M_shp[1], self.M_shp[0]))

        m_gray = cv2.cvtColor(self.Marker_lb[0], cv2.COLOR_BGR2GRAY)
        ret, mask = cv2.threshold(m_gray, 60, 255, cv2.THRESH_BINARY)
        self.marker_mask_inv = cv2.bitwise_not(mask)
        self.Marker_lb[0] = cv2.bitwise_and(self.Marker_lb[0], self.Marker_lb[0], mask=mask)

        m_gray = cv2.cvtColor(self.Marker_lb[1], cv2.COLOR_BGR2GRAY)
        ret, mask = cv2.threshold(m_gray, 95, 255, cv2.THRESH_BINARY)
        self.marker_mask_inv_2 = cv2.bitwise_not(mask)
        self.Marker_lb[1] = cv2.bitwise_and(self.Marker_lb[1], self.Marker_lb[1], mask=self.marker_mask_inv_2)

        return

    def loading(self):
        self.fldr_name = self.informs[0] + ":" + self.informs[1]
        # print(self.fldr_name)
        self.check_points = [[], [], []]
        self.demo_checke_level = [0, 0, 0]
        for i in range(3):
            self.pics[i] = cv2.imread(
                self.path + "/Histories/" + self.informs[2] + "/" + self.fldr_name + "/no_" + str(i) + ".png", 1)
            sso = np.ones((589, 443, 3), np.uint8) * 250
            self.pics[i] = cv2.resize(self.pics[i], (360, 480), interpolation=cv2.INTER_CUBIC)  # (443, 589)
            sso[:480, 41:401, :] = self.pics[i]
            self.pics[i] = sso
            self.pic_h, self.pic_w, c = self.pics[i].shape

            self.checks[i] = np.zeros((self.pic_h, self.pic_w, 1), np.uint8)
            self.lines[i] = np.zeros((self.pic_h, self.pic_w, 1), np.uint8)

        self.gride = np.zeros((self.pic_h, self.pic_w, 1), np.uint8)

        # **************************************************************
        # **************************************************************
        # **************************************************************
        # **************************************************************
        self.grid_x, self.grid_y ,self.grid_col = self.read_gride()
        # **************************************************************
        # **************************************************************
        # **************************************************************
        # **************************************************************



        if not self.no_gride:
            for j in range(1, self.grid_y):
                self.gride = cv2.line(self.gride, (0, j * int(self.pic_h / self.grid_y)),
                                      (self.pic_w, j * int(self.pic_h / self.grid_y)), 60, 1)
            for j in range(1, self.grid_x):
                self.gride = cv2.line(self.gride, (j * int(self.pic_w / self.grid_x), 0),
                                      (j * int(self.pic_w / self.grid_x), self.pic_h), 60, 1)

        self.rett, self.gride = cv2.threshold(self.gride, 10, 255, cv2.THRESH_BINARY)
        self.gride_0 = cv2.bitwise_not(self.gride)

        if os.path.isfile(self.path + "/Histories/" + self.informs[2] + "/" + self.fldr_name + "/anal.txt"):
            # print("NOT NEW")
            self.demo_checke_level = [16, 12, 13]
            self.mark_check()
            self.res_pic_loader()
# ************************************************************************
# ************************************************************************
# ************************************************************************
# ************************************************************************
    def read_gride(self):
        f = open("setting.log",'r')
        s = f.read()

        res = s.split('!')[0]
        res1,res2 = res.split(":")[0],res.split(":")[1]
        if res1 == "0":
            q1 = 40
            q2 = 40
        elif res1 == "1":
            g1 = 30
            q2 = 30
        else:
            g1 = 20
            q2 = 20

        if res2 == "0":
            col = "green"
        else:
            col = "red"

        return q1,q2,col

    # ************************************************************************
    # ************************************************************************
    # ************************************************************************
    # ************************************************************************

    def load_setting(self):
        f = open(self.path + "/setting.log", 'r')
        str = f.read()
        infs = str.split("!")

        self.no_gride = not (bool(int(infs[0])))

        self.voice_on = bool(int(infs[1]))
        f.close()
        # print(self.voice_on)

    def hide(self):
        self.bg.place_forget()

    def moving(self, event):
        x = event.x
        y = event.y
        if ((x < self.touch_margin[0]) or (x > (self.pic_w - self.touch_margin[0])) or (y < self.touch_margin[1]) or (
                y > (self.pic_h - self.touch_margin[1]))): return
        try:
            self.zooming(x, y - 40)
            self.marker_placer(x, y)

        except:

            pass

    def clicked(self, event):
        x = event.x
        y = event.y
        self.chek(x, y - 40)
        self.marker_placer_clicked(x, y)
        # print(x,"::",y)

    def clicking(self, event):
        x = event.x
        y = event.y

        if (x < 120) & (y < 120):
            self.ask()
        elif (x > 24) & (x < (25 + 70)) & (y > 149) & (y < 149 + 70):
            self.back()
        elif (x > 24) & (x < (24 + 70)) & (y > 348) & (y < 348 + 70):
            self.reset()

    def leveling(self, i):
        self.level = i
        self.pic_show(self.level - 1)
        for i in range(3):
            self.demoes[i].place_forget()
            self.txt_lbs[i].place_forget()

        self.txt_lbs[self.level - 1].place(x=420, y=714)
        self.demoes[self.level - 1].place(x=845, y=190)

    def next(self, event):
        # print("NEXT")

        if (self.level == 1) & (len(self.check_points[0]) == len(self.marker_pos[0])):
            self.leveling(2)
            self.marker_nm.set(self.marker_names[1][0])
        elif (self.level == 2) & (len(self.check_points[1]) == len(self.marker_pos[1])):
            self.leveling(3)
            self.marker_nm.set(self.marker_names[2][0])
        elif (self.level == 3) & (len(self.check_points[2]) == len(self.marker_pos[2])):

            self.report()
        #    except ZeroDivisionError:
        #        print("Marker Selection error")
        # print("finished")

    def chek(self, x, y):
        # print(len(self.demo_f_markers),"::",self.demo_checke_level[self.level-1])
        if ((self.level == 1) & (self.demo_checke_level[0] == (len(self.marker_pos[0])))):
            # print("qwee:::",len(self.marker_pos[0]))
            return
        elif (self.level == 2) & (self.demo_checke_level[1] == (len(self.marker_pos[1]))):
            return
        elif (self.level == 3) & (self.demo_checke_level[2] == (len(self.marker_pos[2]))):
            return

        self.voice()

        if self.check_points[self.level - 1] == None:
            self.check_points[self.level - 1] = (x, y)
        else:
            self.check_points[self.level - 1].append((x, y))
        # print(self.check_points[self.level - 1])

        cv2.circle(self.demo_pics[self.level - 1],
                   self.marker_pos[self.level - 1][self.demo_checke_level[self.level - 1]], 4, (0, 200, 50), -1)
        self.marker_update()

        self.demo_checke_level[self.level - 1] += 1
        try:
            self.marker_nm.set(self.marker_names[self.level - 1][self.demo_checke_level[self.level - 1]])
        except:
            pass

        img = self.checks[self.level - 1]
        img = cv2.circle(img, (x, y), 5, 60, -1)
        ret, img = cv2.threshold(img, 10, 255, cv2.THRESH_BINARY)
        self.checks[self.level - 1] = img

        self.liner()

        self.pic_show(self.level - 1)

    def pic_show(self, i):

        self.red = np.zeros((self.pic_h, self.pic_w, 3), np.uint8)
        self.red[:] = (0, 0, 255)
        self.ch_bg = np.zeros((self.pic_h, self.pic_w, 3), np.uint8)
        self.ch_bg[:] = (200, 120, 0)
        self.green = np.zeros((self.pic_h, self.pic_w, 3), np.uint8)
        self.green[:] = (0, 0, 250)

        self.gg = cv2.bitwise_and(self.red, self.red, mask=self.gride)
        self.pc = cv2.bitwise_and(self.pics[i], self.pics[i], mask=self.gride_0)
        self.pic_to_show = cv2.add(self.pc, self.gg)

        if self.checks[i].any() != 0:
            # print("checed")
            mask_1 = cv2.bitwise_and(self.green, self.green, mask=self.lines[i])
            mask_2 = cv2.bitwise_not(self.lines[i])
            self.pic_to_show = cv2.bitwise_and(self.pic_to_show, self.pic_to_show, mask=mask_2)
            self.pic_to_show = cv2.add(self.pic_to_show, mask_1)

            self.checks_o = cv2.bitwise_and(self.ch_bg, self.ch_bg, mask=self.checks[i])
            self.checks_o_o = cv2.bitwise_not(self.checks[i])
            self.pic_to_show = cv2.bitwise_and(self.pic_to_show, self.pic_to_show, mask=self.checks_o_o)
            self.pic_to_show = cv2.add(self.pic_to_show, self.checks_o)

        b, g, r = cv2.split(self.pic_to_show)
        self.ax = cv2.merge((r, g, b))
        self.ax1 = Image.fromarray(self.ax)
        self.ax = ImageTk.PhotoImage(self.ax1)
        self.screen.config(image=self.ax)

    def liner(self):
        if self.level == 1:
            fronter = [1, 3, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15]

            if (self.demo_checke_level[self.level - 1] - 1) in fronter:

                p_num = self.demo_checke_level[self.level - 1] - 1
                img = self.lines[0]

                if (self.demo_checke_level[self.level - 1] - 1) == 12:
                    cv2.line(img, self.check_points[0][12], self.check_points[0][7], 100, 2)

                elif (self.demo_checke_level[self.level - 1] - 1) == 8:
                    cv2.line(img, self.check_points[0][8], self.check_points[0][6], 100, 2)

                else:
                    cv2.line(img, self.check_points[0][p_num], self.check_points[0][p_num - 1], 100, 2)

                ret, img = cv2.threshold(img, 10, 255, cv2.THRESH_BINARY)
                self.lines[0] = img



        elif self.level == 2:
            sider = [1, 3, 5, 6, 8, 9, 10, 11]
            if (self.demo_checke_level[self.level - 1] - 1) in sider:
                p_num = self.demo_checke_level[self.level - 1] - 1
                img = self.lines[1]
                cv2.line(img, self.check_points[1][p_num], self.check_points[1][p_num - 1], 100, 2)
                ret, img = cv2.threshold(img, 10, 255, cv2.THRESH_BINARY)
                self.lines[1] = img

            pass

        elif self.level == 3:
            backer = [2, 4, 5, 6, 8, 10, 12]
            if (self.demo_checke_level[self.level - 1] - 1) in backer:
                p_num = self.demo_checke_level[self.level - 1] - 1
                img = self.lines[2]
                if p_num == 4:
                    cv2.line(img, self.check_points[2][0], self.check_points[2][4], 100, 2)
                elif p_num == 5:
                    cv2.line(img, self.check_points[2][3], self.check_points[2][5], 100, 2)
                elif p_num == 6:
                    cv2.line(img, self.check_points[2][4], self.check_points[2][6], 100, 2)
                else:
                    cv2.line(img, self.check_points[2][p_num - 1], self.check_points[2][p_num], 100, 2)

                ret, img = cv2.threshold(img, 10, 255, cv2.THRESH_BINARY)
                self.lines[2] = img

            pass

        return

    def reset(self):
        self.checks[self.level - 1][:] = 0
        self.pic_show(self.level - 1)
        self.check_points[self.level - 1] = []
        self.demo_checke_level[self.level - 1] = 0
        if self.level == 1:
            self.demo_f_markers[0].config(bg="cyan")
            for i in range(1, len(self.demo_f_markers)):
                self.demo_f_markers[i].config(bg="red")
        elif self.level == 2:
            self.demo_b_markers[0].config(bg="cyan")
            for i in range(1, len(self.demo_b_markers)):
                self.demo_b_markers[i].config(bg="red")
        elif self.level == 3:
            self.demo_s_markers[0].config(bg="cyan")
            for i in range(1, len(self.demo_s_markers)):
                self.demo_s_markers[i].config(bg="red")

                # print(self.checks)

    def back(self):

        q = len(self.check_points[self.level - 1])

        if q < 1: return

        cord = self.check_points[self.level - 1][q - 1]
        self.demo_checke_level[self.level - 1] -= 1

        self.line_cleaner()
        del self.check_points[self.level - 1][q - 1]

        try:
            self.marker_nm.set(self.marker_names[self.level - 1][self.demo_checke_level[self.level - 1]])
        except:
            pass

        cv2.circle(self.demo_pics[self.level - 1],
                   self.marker_pos[self.level - 1][self.demo_checke_level[self.level - 1]], 4, (0, 0, 250), -1)
        sleep(0.5)
        # cv2.circle(self.demo_pics[self.level - 1],
        #            self.marker_pos[self.level - 1][self.demo_checke_level[self.level - 1]], 4, (200, 100, 0), -1)

        b, g, r = cv2.split(self.demo_pics[self.level - 1])
        img = cv2.merge((r, g, b))
        img = Image.fromarray(img)
        self.demo_pics_tk[self.level - 1] = ImageTk.PhotoImage(img)
        self.demoes[self.level - 1].config(image=self.demo_pics_tk[self.level - 1])

        self.erase(cord)

        # print("back")

    def erase(self, cord):
        (x, y) = cord

        img = self.checks[self.level - 1]
        img = cv2.circle(img, (x, y), 5, 5, -1)
        ret, img = cv2.threshold(img, 10, 255, cv2.THRESH_BINARY)

        self.checks[self.level - 1] = img

        self.pic_show(self.level - 1)

    def marker_update(self):

        b, g, r = cv2.split(self.demo_pics[self.level - 1])
        img = cv2.merge((r, g, b))
        img = Image.fromarray(img)
        self.demo_pics_tk[self.level - 1] = ImageTk.PhotoImage(img)
        self.demoes[self.level - 1].config(image=self.demo_pics_tk[self.level - 1])

    def marker_spladsher(self, j):
        if (self.demo_checke_level[self.level - 1]) <= (len(self.marker_pos[self.level - 1]) - 1):
            point = self.marker_pos[self.level - 1][self.demo_checke_level[self.level - 1]]
            if j:
                cv2.circle(self.demo_pics[self.level - 1], point, 4, (250, 150, 10), -1)
            else:
                cv2.circle(self.demo_pics[self.level - 1], point, 4, (0, 0, 250), -1)

        self.marker_update()

        self.demoes[self.level - 1].after(500, self.marker_spladsher, (not j))

    def report(self):
        self.saving()
        res = self.forms[6].show(self.forms, self.informs)
        if res != True:
            if res == 'F':
                self.prompt.config(text="Front Marking Error")
            elif res == 'B':
                self.prompt.config(text="Back Marking Error")
            else:
                self.prompt.config(text="Side Marking Error")

            self.prompt.place(x=(1024 - 500) / 2, y=(768 - 300) / 2, width=500, height=300)
            self.bg.after(3000, lambda: self.prompt.place_forget())

            return

        self.hide()

    def saving(self):
        f = open(self.path + "/Histories/" + self.informs[2] + "/" + self.fldr_name + "/anal.txt", 'w')
        st = ["", "", ""]
        for i in range(3):
            for j in range(len(self.check_points[i])):
                st[i] = st[i] + ":" + str(self.check_points[i][j][0]) + "," + str(self.check_points[i][j][1])

        st_f = "--f--" + st[0] + "--b--" + st[1] + "--s--" + st[2]
        f.write(st_f)
        f.close()
        for i in range(3):
            cv2.imwrite(
                self.path + "/Histories/" + self.informs[2] + "/" + self.fldr_name + "/markers_" + str(i) + ".png",
                self.checks[i])
            cv2.imwrite(
                self.path + "/Histories/" + self.informs[2] + "/" + self.fldr_name + "/lines_" + str(i) + ".png",
                self.lines[i])
        return

    def voice(self):
        if self.voice_on:
            duration = 0.2  # second
            freq = 1000  # Hz
            os.system('play --no-show-progress --null --channels 1 synth %s sine %f' % (duration, freq))

        return

    def zoomer_init(self):

        pics = cv2.imread(self.path + "/pics/analys/zoom.png", 1)
        pics1 = cv2.imread(self.path + "/pics/analys/zoom-1.png", 1)
        pics2 = cv2.imread(self.path + "/pics/analys/zoom-2.png", 1)
        # print(pics.shape())
        bl = np.zeros((330, 330, 3), np.uint8)
        bl[:] = (200, 200, 200)

        pics1 = cv2.cvtColor(pics1, cv2.COLOR_BGR2GRAY)

        pic_to_show_1 = cv2.bitwise_and(bl, bl, mask=pics1)

        pics2 = cv2.cvtColor(pics2, cv2.COLOR_BGR2GRAY)

        pic_to_show_2 = cv2.bitwise_and(pics, pics, mask=pics2)

        pic_to_show = cv2.add(pic_to_show_1, pic_to_show_2)
        pics = pic_to_show

        img = pics
        pic = Image.fromarray(img)
        pic_tk = ImageTk.PhotoImage(pic)

        return pic_tk

    def zooming(self, x, y):

        self.img1 = self.pics[self.level - 1][y - 55:y + 55, x - 55:x + 55]
        height, width = self.img1.shape[:2]
        try:
            self.img2 = cv2.resize(self.img1, (3 * width, 3 * height), interpolation=cv2.INTER_CUBIC)
            self.img2 = cv2.line(self.img2, (160, 165), (170, 165), (0, 0, 255), 2)
            self.img2 = cv2.line(self.img2, (165, 160), (165, 170), (0, 0, 255), 2)

            pics = cv2.imread(self.path + "/pics/analys/zoom.png", 1)
            pics1 = cv2.imread(self.path + "/pics/analys/zoom-1.png", 1)
            pics2 = cv2.imread(self.path + "/pics/analys/zoom-2.png", 1)
            bl = self.img2
            pics1 = cv2.cvtColor(pics1, cv2.COLOR_BGR2GRAY)
            pic_to_show_1 = cv2.bitwise_and(bl, bl, mask=pics1)
            pics2 = cv2.cvtColor(pics2, cv2.COLOR_BGR2GRAY)
            pic_to_show_2 = cv2.bitwise_and(pics, pics, mask=pics2)
            self.img2 = cv2.add(pic_to_show_1, pic_to_show_2)
            b, g, r = cv2.split(self.img2)
            self.zax = cv2.merge((r, g, b))
            self.zax = Image.fromarray(self.zax)
            self.zax = ImageTk.PhotoImage(self.zax)
            self.zoom.config(image=self.zax)
        except cv2.error:
            pass

        return

    def marker_placer(self, x, y):
        pos = (y - 40, x - 34)

        selected_bg = self.pic_to_show[pos[0]:pos[0] + self.M_shp[0], pos[1]:pos[1] + self.M_shp[1]]
        selected_bg = cv2.bitwise_and(selected_bg, selected_bg, mask=self.marker_mask_inv)
        dst = cv2.add(selected_bg, self.Marker_lb[0])
        self.picsp = self.pic_to_show.copy()
        self.picsp[pos[0]:pos[0] + self.M_shp[0], pos[1]:pos[1] + self.M_shp[1]] = dst

        b, g, r = cv2.split(self.picsp)
        self.ax = cv2.merge((r, g, b))
        self.ax1 = Image.fromarray(self.ax)
        self.ax = ImageTk.PhotoImage(self.ax1)
        self.screen.config(image=self.ax)

        return

    def marker_placer_clicked(self, x, y):
        pos = (y - 40, x - 34)

        selected_bg = self.pic_to_show[pos[0]:pos[0] + self.M_shp[0], pos[1]:pos[1] + self.M_shp[1]]
        selected_bg = cv2.bitwise_and(selected_bg, selected_bg, mask=self.marker_mask_inv)

        dst = cv2.add(selected_bg, self.Marker_lb[1])

        self.picsp = self.pic_to_show.copy()
        self.picsp[pos[0]:pos[0] + self.M_shp[0], pos[1]:pos[1] + self.M_shp[1]] = dst

        b, g, r = cv2.split(self.picsp)
        self.ax = cv2.merge((r, g, b))
        self.ax1 = Image.fromarray(self.ax)
        self.ax = ImageTk.PhotoImage(self.ax1)
        self.screen.config(image=self.ax)

        return

    def mark_check(self):
        # self.marks=[[],[],[]]
        f = open(self.path + "/Histories/" + self.informs[2] + "/" + self.fldr_name + "/anal.txt", 'r')
        st = f.read()
        s = st.split('--')
        del s[0]
        del s[0]
        del s[1]
        del s[2]
        self.marks = [[], [], []]
        for i in range(3):
            fs = s[i].split(":")
            del fs[0]
            for k in range(len(fs)):
                ss = fs[k].split(',')
                self.marks[i].append((int(ss[0]), int(ss[1])))
        self.check_points = self.marks
        return

    def res_pic_loader(self):

        for i in range(3):
            self.checks[i] = cv2.imread(
                self.path + "/Histories/" + self.informs[2] + "/" + self.fldr_name + "/markers_" + str(i) + ".png", -1)
            self.lines[i] = cv2.imread(
                self.path + "/Histories/" + self.informs[2] + "/" + self.fldr_name + "/lines_" + str(i) + ".png", -1)
        self.pic_show(0)
        return

    def line_cleaner(self):
        if self.level == 1:
            fronter = [1, 3, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15]

            if (self.demo_checke_level[self.level - 1]) in fronter:

                p_num = self.demo_checke_level[self.level - 1]
                img = self.lines[0]

                if (self.demo_checke_level[self.level - 1]) == 12:
                    cv2.line(img, self.check_points[0][12], self.check_points[0][7], 0, 2)

                elif (self.demo_checke_level[self.level - 1]) == 8:
                    cv2.line(img, self.check_points[0][8], self.check_points[0][6], 0, 2)

                else:
                    cv2.line(img, self.check_points[0][p_num], self.check_points[0][p_num - 1], 0, 2)

                ret, img = cv2.threshold(img, 10, 255, cv2.THRESH_BINARY)
                self.lines[0] = img



        elif self.level == 2:
            sider = [1, 3, 5, 6, 8, 9, 10, 11]
            if (self.demo_checke_level[self.level - 1]) in sider:
                p_num = self.demo_checke_level[self.level - 1]
                img = self.lines[1]
                cv2.line(img, self.check_points[1][p_num], self.check_points[1][p_num - 1], 0, 2)
                ret, img = cv2.threshold(img, 10, 255, cv2.THRESH_BINARY)
                self.lines[1] = img

            pass

        elif self.level == 3:
            backer = [2, 4, 5, 6, 8, 10, 12]
            if (self.demo_checke_level[self.level - 1]) in backer:
                p_num = self.demo_checke_level[self.level - 1]
                img = self.lines[2]
                if p_num == 4:
                    cv2.line(img, self.check_points[2][0], self.check_points[2][4], 0, 2)
                elif p_num == 5:
                    cv2.line(img, self.check_points[2][3], self.check_points[2][5], 0, 2)
                elif p_num == 6:
                    cv2.line(img, self.check_points[2][4], self.check_points[2][6], 0, 2)
                else:
                    cv2.line(img, self.check_points[2][p_num - 1], self.check_points[2][p_num], 0, 2)

                ret, img = cv2.threshold(img, 10, 255, cv2.THRESH_BINARY)
                self.lines[2] = img

            pass

        return

        return
