import cv2
import numpy as np
from PIL import Image,ImageTk
import sys

w = 680
h = 924
sample_path="/home/hossein/Desktop/bddd/Histories/1396-11-25/HOS-TEST-2:55884433/"


red = np.zeros((h, w, 3), np.uint8)
red[:] = (0, 0, 255)

ch_bg = np.zeros((h, w,3), np.uint8)
ch_bg[:] = (200, 120, 0)

green = np.zeros((h, w, 3), np.uint8)
green[:] = (0, 250, 0)

res =  [{'shoulder': (7.9000000000000004, 'Elevated R'), 'ribcage': (0.0, 'Normal'), 'pelvis': (3.6000000000000001, 'Level'), 'rightknee': (-63.399999999999999, 'Severe Genu Valgum'), 'leftknee': (-31.800000000000001, 'Severe Genu Valgum'), 'head': (-9.5, 'Tilted L'), 'rightfoot': (84.299999999999997, 'External Tibial Torsion'), 'leftfoot': (8.5, 'External Tibial Torsion')}, {'cervicalspine': (-9.5, 'Flat'), 'knees': (32.299999999999997, 'Flexed'), 'upperthoracic': (-5.8, 'Flat'), 'lumberspine': (-17.0, 'Flat'), 'pelvis': (-0.4, 'Posterior Pelvic Tilt')}, {'rightfoot': (-20.5, 'Supinated'), 'pelvis': (3.7, 'Level'), 'leftfoot': (-43.0, 'Supinated'), 'scapulae': (-10.0, 'Elevated R'), 'thoracic': (4.9000000000000004, 'Normal'), 'humeri': (-20.4, 'Medially Rotated R')}]
infs=['HOS-TEST-2', '55884433', '1396-11-25']



def Maker(mode,infs,res):

    bg=cv2.imread(sys.path[0]+"/pics/A4_bg.jpeg")
    bg=cv2.resize(bg,(2480,3508))

    fldr_name = infs[0] + ":" + infs[1]
    path=sys.path[0]+"/Histories/" + infs[2] + "/"+fldr_name+"/"
    # path=sample_path

    Name=infs[0]
    Id=infs[1]
    Date=infs[2]

    pic=["","",""]
    marks=["","",""]
    lines=["","",""]
    pic_f=["","",""]
    
    A4=bg

    if mode == 1:

        for i in range(3):
            pic[i]=cv2.imread(path+"no_"+str(i)+".png")
            pic[i]=cv2.resize(pic[i],(w,h))  # 443,589
            marks[i]=cv2.imread(path+"markers_"+str(i)+".png",-1)
            lines[i]=cv2.imread(path+"lines_"+str(i)+".png",-1)
            marks[i]=cv2.resize(marks[i],(w, h))
            lines[i] = cv2.resize(lines[i], (w, h))

        for i in range(3):
            
            mask_1 = cv2.bitwise_and(green,green, mask=lines[i])
            mask_2 = cv2.bitwise_not(lines[i])
            pic_to_show = cv2.bitwise_and(pic[i],pic[i], mask=mask_2)
            pic_to_show = cv2.add(pic_to_show, mask_1)


            mask_1 = cv2.bitwise_and(red,red, mask=marks[i])
            mask_2 = cv2.bitwise_not(marks[i])
            pic_to_show = cv2.bitwise_and(pic_to_show,pic_to_show, mask=mask_2)
            pic_to_show = cv2.add(pic_to_show, mask_1)

            pic_f[i]=pic_to_show

    elif mode==2:

        for i in range(3):
            pic[i] = cv2.imread(path + "no_" + str(i) + ".png")
            pic[i] = cv2.resize(pic[i], (w, h))  # 443,589
            pic[i]=inverte(pic[i])
            marks[i] = cv2.imread(path + "markers_" + str(i) + ".png", -1)
            lines[i] = cv2.imread(path + "lines_" + str(i) + ".png", -1)
            marks[i] = cv2.resize(marks[i], (w, h))
            lines[i] = cv2.resize(lines[i], (w, h))

        for i in range(3):
            mask_1 = cv2.bitwise_and(green, green, mask=lines[i])
            mask_2 = cv2.bitwise_not(lines[i])
            pic_to_show = cv2.bitwise_and(pic[i], pic[i], mask=mask_2)
            pic_to_show = cv2.add(pic_to_show, mask_1)

            mask_1 = cv2.bitwise_and(red, red, mask=marks[i])
            mask_2 = cv2.bitwise_not(marks[i])
            pic_to_show = cv2.bitwise_and(pic_to_show, pic_to_show, mask=mask_2)
            pic_to_show = cv2.add(pic_to_show, mask_1)

            pic_f[i] = pic_to_show
    else:

        for i in range(3):
            # pic[i] = cv2.imread(path + "no_" + str(i) + ".png")
            # pic[i] = cv2.resize(pic[i], (w, h))  # 443,589
            pic[i]=np.zeros((h, w, 3), np.uint8)
            marks[i] = cv2.imread(path + "markers_" + str(i) + ".png", -1)
            lines[i] = cv2.imread(path + "lines_" + str(i) + ".png", -1)
            marks[i] = cv2.resize(marks[i], (w, h))
            lines[i] = cv2.resize(lines[i], (w, h))

        for i in range(3):
            mask_1 = cv2.bitwise_and(green, green, mask=lines[i])
            mask_2 = cv2.bitwise_not(lines[i])
            pic_to_show = cv2.bitwise_and(pic[i], pic[i], mask=mask_2)
            pic_to_show = cv2.add(pic_to_show, mask_1)

            mask_1 = cv2.bitwise_and(red, red, mask=marks[i])
            mask_2 = cv2.bitwise_not(marks[i])
            pic_to_show = cv2.bitwise_and(pic_to_show, pic_to_show, mask=mask_2)
            pic_to_show = cv2.add(pic_to_show, mask_1)

            pic_f[i] = pic_to_show

    font=0

    cv2.putText(A4,Name,(1040,230),font,1.5,(0,0,0),8)
    cv2.putText(A4,Date, (2020, 300), font, 1.5, (0, 0, 0), 8)
    cv2.putText(A4, Id, (1140, 300), font, 1.5, (0, 0, 0), 8)

    ##values##
    col=(100,50,50)
    cv2.putText(A4, str(res[1]["cervicalspine"][0]), (980, 1690), font, 2, col, 8)
    cv2.putText(A4, str(res[0]['shoulder'][0]), (980, 1830), font, 2, col, 8)
    cv2.putText(A4, str(res[1]['upperthoracic'][0]), (980, 1970), font, 2, col, 8)
    cv2.putText(A4, str(res[1]['lumberspine'][0]), (980, 2110), font, 2, col, 8)
    cv2.putText(A4, str(res[2]['scapulae'][0]), (980, 2250), font, 2, col, 8)
    cv2.putText(A4, str(res[2]['thoracic'][0]), (980, 2390), font, 2, col, 8)
    cv2.putText(A4, str(res[0]["pelvis"][0]), (980, 2530), font, 2, col, 8)
    cv2.putText(A4, str(round(res[0]['rightfoot'][0], 1)) + "/" + str(round(res[0]['leftfoot'][0], 1)), (900, 2670), font, 2, col, 8)
    cv2.putText(A4, str(round(res[0]['rightknee'][0], 1))+"/"+str(round(res[0]['leftknee'][0], 1)), (900, 2810), font, 2, col, 8)
    cv2.putText(A4, str(round(res[2]['rightfoot'][0], 1)) + "/" + str(round(res[2]['leftfoot'][0], 1)), (900, 2950), font, 2, col, 8)

    ##State##
    col = (100, 100, 50)
    cv2.putText(A4, res[1]["cervicalspine"][1], (1450, 1690), font, 2, col, 8)
    cv2.putText(A4, res[0]['shoulder'][1], (1450, 1830), font, 2, col, 8)
    cv2.putText(A4, res[1]['upperthoracic'][1], (1450, 1970), font, 2, col, 8)
    cv2.putText(A4, res[1]['lumberspine'][1], (1450, 2110), font, 2, col, 8)
    cv2.putText(A4, res[2]['scapulae'][1], (1450, 2250), font, 2, col, 8)
    cv2.putText(A4, res[2]['thoracic'][1], (1450, 2390), font, 2, col, 8)
    cv2.putText(A4, res[0]["pelvis"][1], (1450, 2530), font, 2, col, 8)
    cv2.putText(A4, res[0]['rightfoot'][1] + "/" + res[0]['leftfoot'][1], (1400, 2670), font, 1.2, col, 6)
    cv2.putText(A4, res[0]['rightknee'][1] + "/" + res[0]['leftknee'][1], (1400, 2810), font, 1.2, col, 6)
    cv2.putText(A4, res[2]['rightfoot'][1] + "/" + res[2]['leftfoot'][1], (1400, 2950), font, 2, col, 8)

    p1=[220,465]
    p3=[930,465]
    p2=[1647,465]

    A4[p1[1]:p1[1]+h,p1[0]:p1[0]+w,:]=pic_f[0]
    
    A4[p2[1]:p2[1]+h,p2[0]:p2[0]+w,:]=pic_f[1]

    A4[p3[1]:p3[1]+h,p3[0]:p3[0]+w,:]=pic_f[2]



    # cv2.imwrite('/home/hossein/Desktop/Final.png',A4)


    return A4

def inverte(imagem):
    imagem = (255-imagem)
    return imagem

# Maker(1,infs,res)
