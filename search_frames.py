from  tkinter import *
import tkinter.ttk as ttk
import sys

class main_plan(Label):
    def load(self):
        self.path = sys.path[0]
        self.bg_pic=PhotoImage(file=self.path+"/pics/history/simple_tree_2.png")
        # self.bg=Label(self,image=self.bg_pic)
        self.config(image=self.bg_pic)
        self.place(x=393,y=58+38,width=592,height=(704-58))#x=393,y=38)

        style = ttk.Style()

        style.configure("mystyle.Treeview", highlightthickness=0, bd=0,
                        font=('Calibri', 13,'bold'),fg="darkred")
        self.tree=ttk.Treeview(self, height=8, columns=('no-1', 'no2','n0-3'), selectmode="browse",show="",style="mystyle.Treeview")
        self.vsb = Scrollbar(self,orient="vertical", command=self.tree.yview)
        self.tree.configure(yscrollcommand=self.vsb.set)

        self.tree.column("#0", width=193,anchor=CENTER)
        self.tree.column("#1", width=193,anchor=CENTER)
        self.tree.column("#2", width=197,anchor=CENTER)
        self.tree.column("#3", width=192, anchor=CENTER)



        self.tree.place(x=4,y=0,width=592,height=704-58)
        self.vsb.place(x=592 - 30, y=0, width=30, height=704 - 58)
        return


    def list_loader(self,Data):
        for i in Data: self.tree.insert("", 'end', text=str(i), values=(i[0],i[1], i[2]))
        return

    def unloader(self):
        self.place_forget()
        return




class Date_plan(Label):
    def load(self,keyboard):
        self.keyboard=keyboard

        self.path = sys.path[0]
        self.bg_pic=PhotoImage(file=self.path+"/pics/history/Date_tree_2.png")
        self.config(image=self.bg_pic)

        self.place(x=393,y=58+38,width=592,height=(704-58))#x=393,y=38)

        style = ttk.Style()
        style.configure("mystyle.Treeview", highlightthickness=0, bd=0,
                        font=('Calibri', 13,'bold'),fg="darkred")
        self.tree=ttk.Treeview(self, height=8, columns=('no-1', 'no2','n0-3'), selectmode="browse",show="",style="mystyle.Treeview")
        self.vsb = Scrollbar(self, orient="vertical", command=self.tree.yview)
        self.tree.configure(yscrollcommand=self.vsb.set)

        self.tree.column("#0", width=193,anchor=CENTER)
        self.tree.column("#1", width=193,anchor=CENTER)
        self.tree.column("#2", width=197,anchor=CENTER)
        self.tree.column("#3", width=192, anchor=CENTER)

        self.txtvars=[StringVar(), StringVar(), StringVar(),StringVar(), StringVar(), StringVar()]

        self.txt_lbs=[Label(self, bg="white", textvariable=self.txtvars[0]),
                      Label(self, bg="white", textvariable=self.txtvars[1]),
                      Label(self, bg="white", textvariable=self.txtvars[2]),
                      Label(self, bg="white", textvariable=self.txtvars[3]),
                      Label(self, bg="white", textvariable=self.txtvars[4]),
                      Label(self, bg="white", textvariable=self.txtvars[5])]

        self.txt_lbs[0].place(x=205, y=51, width=116, height=33)
        self.txt_lbs[1].place(x=337, y=51, width=116, height=33)
        self.txt_lbs[2].place(x=467, y=51, width=116, height=33)
        self.txt_lbs[3].place(x=205, y=98, width=116, height=33)
        self.txt_lbs[4].place(x=337, y=98, width=116, height=33)
        self.txt_lbs[5].place(x=467, y=98, width=116, height=33)

        self.txt_lbs[0].bind("<1>", lambda x: self.inner(0))
        self.txt_lbs[1].bind("<1>", lambda x: self.inner(1))
        self.txt_lbs[2].bind("<1>", lambda x: self.inner(2))
        self.txt_lbs[3].bind("<1>", lambda x: self.inner(3))
        self.txt_lbs[4].bind("<1>", lambda x: self.inner(4))
        self.txt_lbs[5].bind("<1>", lambda x: self.inner(5))

        self.tree.place(x=4, y=146, width=592, height=(704 - 58-146))
        self.vsb.place(x=592 - 30, y=146, width=30, height=704 - 58-146)
        self.d_data = ""
        return

    def inner(self,i):
        print("inner--- ",i)
        for j in range(6):self.txt_lbs[j].config(bg="white")
        self.txt_lbs[i].config(bg="#a6a9b0")
        self.keyboard.var=self.txtvars[i]
        self.keyboard.refrencer(self)
        self.keyboard.placer()


        return

    def list_loader(self,Data):
        self.d_data=Data
        for i in Data: self.tree.insert("", 'end', text=str(i), values=(i[0],i[1], i[2]))
        return

    def unloader(self):
        self.place_forget()
        self.keyboard.unplacer()
        return

    def main_sorter(self):
        self.t_data = list()
        if (self.txtvars[3].get()=="") & (self.txtvars[4].get()=="") & (self.txtvars[5].get()==""):
            if(self.txtvars[0].get()==""):
                self.t_data=self.d_data
            elif (self.txtvars[0].get()!=""):

                for i in self.d_data:
                    if i[0].split("-")[0]==self.txtvars[0].get():self.t_data.append(i)

                if (self.txtvars[1].get()!=""):
                    t1=list()
                    for i in self.t_data:
                        if i[0].split("-")[1]==self.txtvars[1].get():t1.append(i)
                    self.t_data=t1
                    if (self.txtvars[2].get()!=""):
                        t2=list()
                        for i in t1:
                            if i[0].split("-")[2]==self.txtvars[2].get():t2.append(i)
                        self.t_data=t2

        else:
            y=int(self.txtvars[3].get())
            y0=int(self.txtvars[0].get())
            # m=int(self.txtvars[4].get())
            # d=int(self.txtvars[5].get())
            for i in self.d_data:
                if (int(i[0].split("-")[0])<=y) & (int(i[0].split("-")[0])>=int(self.txtvars[0].get())):
                    self.t_data.append(i)
            if self.txtvars[4].get()!="":
                m = int(self.txtvars[4].get())
                m0 = int(self.txtvars[1].get())
                for i in self.t_data:
                    if ((int(i[0].split("-")[0])==y)&(int(i[0].split("-")[1])>m)) or ((int(i[0].split("-")[0])==y0)&(int(i[0].split("-")[1])<m0)):
                        self.t_data.remove(i)

                if self.txtvars[5].get()!="":
                    d = int(self.txtvars[5].get())
                    d0 = int(self.txtvars[2].get())
                    for i in self.t_data:
                        if ((int(i[0].split("-")[0])==y)&(int(i[0].split("-")[1])==m)&(int(i[0].split("-")[2])>d)) or ((int(i[0].split("-")[0])==y0)&(int(i[0].split("-")[1])==m0)&(int(i[0].split("-")[2])<d0)):
                            self.t_data.remove(i)

        for i in self.tree.get_children():self.tree.delete(i)
        for i in self.t_data: self.tree.insert("", 'end', text=str(i), values=(i[0], i[1], i[2]))


        return




class Name_plan(Label):
    def load(self,keyboard):
        self.keyboard=keyboard
        self.path = sys.path[0]
        self.bg_pic=PhotoImage(file=self.path+"/pics/history/Name_tree_2.png")
        self.config(image=self.bg_pic)

        self.place(x=393,y=58+38,width=592,height=(704-58))#x=393,y=38)

        style = ttk.Style()
        style.configure("mystyle.Treeview", highlightthickness=0, bd=0,
                        font=('Calibri', 13,'bold'),fg="darkred")
        self.tree=ttk.Treeview(self, height=8, columns=('no-1', 'no2','n0-3'), selectmode="browse",show="",style="mystyle.Treeview")
        self.vsb = Scrollbar(self, orient="vertical", command=self.tree.yview)
        self.tree.configure(yscrollcommand=self.vsb.set)

        self.tree.column("#0", width=193,anchor=CENTER)
        self.tree.column("#1", width=193,anchor=CENTER)
        self.tree.column("#2", width=197,anchor=CENTER)
        self.tree.column("#3", width=192, anchor=CENTER)

        self.txt=StringVar()
        self.txt_lb=Label(self,bg="white", textvariable=self.txt)
        self.txt_lb.place(x=272,y=58,width=281,height=44)
        self.txt_lb.bind("<1>", lambda x: self.inner())

        self.tree.place(x=4, y=146, width=592, height=(704 - 58-146))
        self.vsb.place(x=592 - 30, y=146, width=30, height=704 - 58-146)

        return
    def unloader(self):
        self.place_forget()
        try:
            self.keyboard.unplacer()
        except:
            pass
        return

    def inner(self):
        self.txt_lb.config(bg="#a6a9b0")
        self.keyboard.var=self.txt
        self.keyboard.refrencer(self)
        self.keyboard.placer()


        return

    def list_loader(self,Data):
        self.d_data=Data
        for i in Data: self.tree.insert("", 'end', text=str(i), values=(i[0],i[1], i[2]))
        return

    def main_sorter(self):
        q=self.txt.get()
        self.t_data=list()
        for i in self.d_data:
            if i[1].find(q)!=-1: self.t_data.append(i)

        for i in self.tree.get_children(): self.tree.delete(i)
        for i in self.t_data: self.tree.insert("", 'end', text=str(i), values=(i[0], i[1], i[2]))

        return

class Id_plan(Label):
    def load(self,keyboard):
        self.keyboard=keyboard
        self.path = sys.path[0]
        self.bg_pic=PhotoImage(file=self.path+"/pics/history/id_tree_2.png")
        self.config(image=self.bg_pic)

        self.place(x=393,y=58+38,width=592,height=(704-58))#x=393,y=38)

        style = ttk.Style()
        style.configure("mystyle.Treeview", highlightthickness=0, bd=0,
                        font=('Calibri', 13,'bold'),fg="darkred")
        self.tree=ttk.Treeview(self, height=8, columns=('no-1', 'no2','n0-3'), selectmode="browse",show="",style="mystyle.Treeview")
        self.vsb = Scrollbar(self, orient="vertical", command=self.tree.yview)
        self.tree.configure(yscrollcommand=self.vsb.set)

        self.tree.column("#0", width=193,anchor=CENTER)
        self.tree.column("#1", width=193,anchor=CENTER)
        self.tree.column("#2", width=197,anchor=CENTER)
        self.tree.column("#3", width=192, anchor=CENTER)

        self.txt = StringVar()
        self.txt_lb = Label(self, bg="white", textvariable=self.txt)
        self.txt_lb.place(x=205, y=60, width=345, height=41)
        self.txt_lb.bind("<1>", lambda x: self.inner())


        self.tree.place(x=4, y=146, width=592, height=(704 - 58-146))
        self.vsb.place(x=592 - 30, y=146, width=30, height=704 - 58-146)

        return

    def inner(self):
        self.txt_lb.config(bg="#a6a9b0")
        self.keyboard.var=self.txt
        self.keyboard.refrencer(self)
        self.keyboard.placer()


        return

    def list_loader(self,Data):
        self.d_data=Data
        for i in Data: self.tree.insert("", 'end', text=str(i), values=(i[0],i[1], i[2]))
        return
    def unloader(self):
        self.place_forget()
        try:
            self.keyboard.unplacer()
        except:
            pass
        return
    def main_sorter(self):
        q=self.txt.get()
        self.t_data=list()
        for i in self.d_data:
            if i[2].find(q)!=-1: self.t_data.append(i)

        for i in self.tree.get_children(): self.tree.delete(i)
        for i in self.t_data: self.tree.insert("", 'end', text=str(i), values=(i[0], i[1], i[2]))

        return
