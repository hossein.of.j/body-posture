#for installing openccv "sudo pip3 install opencv-python"
import cv2
from tkinter import *
from PIL import Image, ImageTk
import numpy as np
from threading import Timer


class Screen_label(Label):
    
    def load(self):
        try:
            if not self.cam.isOpened():
                print("cam is open")
                self.cam.release()
                self.cam.open(0)
                
                return
        except:
            pass
        self.cam=cv2.VideoCapture(0)
        self.online=TRUE
        self.cam.set(3, 640)
        self.cam.set(4, 480)
        self.config(bg="white")




    def capture(self):
        self.stop()
        ret, frame = self.cam.read()
        row,col,c=frame.shape

        # frame2=np.zeros((col,row,3),np.uint8)

        # for i in range(row):
        #     frame2[:,i]=frame[i,:]

        frame = frame[:, 15:465]
        res = cv2.resize(frame, (351, 465), interpolation=cv2.INTER_CUBIC)
        croped_img = res
        # croped_img=cv2.flip(croped_img,0)
        # croped_img = cv2.flip(croped_img, 1)
        # croped_img=self.invrt(croped_img)

        cv2.imwrite("home/hossein/Desktop/c_test.png",croped_img)

        b, g, r = cv2.split(croped_img)
        self.cp=croped_img
        self.img = cv2.merge((r, g, b))
        self.im = Image.fromarray(self.img)
        self.imgtk = ImageTk.PhotoImage(self.im)
        self.config(image=self.imgtk)



        self.after(700,self.resume)
        print("captured")
        return self.im


    def resume(self):
        self.online=True
        self.stream()
        return

    def stream(self):
        if (not self.online):
            print("notOnline")
            return

        ret, frame = self.cam.read()


        # height, width = frame.shape[:2]
        try:
            frame=frame[:,15:465]
        except:
            self.after(40,self.stream)
            return
        res = cv2.resize(frame, (351, 465), interpolation=cv2.INTER_CUBIC)
        croped_img=res

        (h,w) = croped_img.shape[:2]
        center = (w/2, h/2)

        angle = 270

        scale = 1
        M = cv2.getRotationMatrix2D(center,angle,scale)
        rotated = cv2.warpAffine(croped_img,M,(w,h))
        croped_img = rotated
        # croped_img=cv2.flip(croped_img,0)
        # croped_img = cv2.flip(croped_img, 1)
        # croped_img=self.invrt(croped_img)
        b, g, r = cv2.split(croped_img)

        self.img = cv2.merge((r, g, b))
        self.im = Image.fromarray(self.img)
        self.imgtk = ImageTk.PhotoImage(self.im)

        self.config(image=self.imgtk)
        self.after(40,self.stream)
        return
        # self.timer = Timer(0.08, self.stream, args=[])
        # self.timer.start()


    def start(self):
        
        self.online=True
        #if not self.cam.isOpened():
        #    self.cam.release()
        #    self.cam.open(0)
        print("Started")
        self.after(100,self.stream)
        #Timer(0.1,self.stream,args=[]).start()


    def stop(self):
        print("Stopped")
        self.online=False

    def close(self):

        self.stop()
        self.cam.release()

    def invrt(self,img):
        img = (255 - img)
        return img

    def circle(self,event):
        img=self.cp
        img=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        circles = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, 1, 20,
                                   param1=50, param2=30, minRadius=0, maxRadius=0)
        print(circles)

    def set_pic(self,pic):
        self.pic=ImageTk.PhotoImage(pic)

        self.config(image=self.pic)


        return
