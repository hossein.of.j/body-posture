import cv2
import numpy as np


def front_analyzer(points):
    if len(points) != 16:
        return "ERROR"

    (eyes) = (points[0], points[1])

    (acromion_l, acromion_r) = (points[3], points[2])

    (rib_cage_t, rib_cage_b) = (points[4], points[5])

    (asis_l, asis_r) = (points[7], points[6])

    q_angle_r = [points[6], points[8], points[9]]

    q_angle_l = [points[7], points[12], points[13]]

    tibia_l = [points[14], points[15]]
    tibia_r = [points[10], points[11]]

    acromion = ((acromion_r[1] - acromion_l[1]) / (acromion_r[0] - acromion_l[0])) * (-1)
    rib_cage = (rib_cage_b[0] - rib_cage_t[0]) / (rib_cage_b[1] - rib_cage_t[1])
    asis = (asis_r[1] - asis_l[1]) / (asis_r[0] - asis_l[0])
    l_tibia = ((tibia_l[1][0] - tibia_l[0][0]) / (tibia_l[1][1] - tibia_l[0][1])) * (-1)
    r_tibia = (tibia_r[1][0] - tibia_r[0][0]) / (tibia_r[1][1] - tibia_r[0][1])

    q1 = (q_angle_l[1][0] - q_angle_l[0][0]) / (q_angle_l[1][1] - q_angle_l[0][1])
    qa1 = np.arctan(q1)
    qa1 = np.rad2deg(qa1)
    q2 = (q_angle_l[2][0] - q_angle_l[1][0]) / (q_angle_l[2][1] - q_angle_l[1][1])
    qa2 = np.arctan(q2)
    qa2 = np.rad2deg(qa2)

    q_angle_l_f = qa2 - qa1

    q1 = (q_angle_r[1][0] - q_angle_r[0][0]) / (q_angle_r[1][1] - q_angle_r[0][1])
    qa1 = np.arctan(q1)
    qa1 = np.rad2deg(qa1)
    q2 = (q_angle_r[2][0] - q_angle_r[1][0]) / (q_angle_r[2][1] - q_angle_r[1][1])
    qa2 = np.arctan(q2)
    qa2 = np.rad2deg(qa2)
    q_angle_r_f = (qa2 - qa1) * (-1)
    rib_cage = np.arctan(rib_cage)
    eye = ((eyes[1][1] - eyes[0][1]) / (eyes[1][0] - eyes[0][0]))
    angs = [eye, acromion, asis, r_tibia, l_tibia]
    angles = np.arctan(angs)
    angles = np.rad2deg(angles)

    results = {'head': int(angles[0]), 'shoulder': int(angles[1]), 'ribcage': int(rib_cage), 'rightknee': int(q_angle_r_f),
               'leftknee': int(q_angle_l_f),
               'rightfoot': int(angles[3]), 'leftfoot': int(angles[4]), 'pelvis': int(angles[2])}

    return results


def front_shaper(points):
    if len(points) != 16:
        return "ERROR"

    (eye_l, eye_r) = (points[0], points[1])
    (acromion_l, acromion_r) = (points[2], points[3])
    (rib_cage_t, rib_cage_b) = (points[4], points[5])
    (asis_l, asis_r) = (points[6], points[7])
    q_angle_r = [points[7], points[9], points[11]]
    q_angle_l = [points[6], points[8], points[10]]
    tibia_l = [points[12], points[14]]
    tibia_r = [points[13], points[15]]

    pic = np.zeros((640, 480, 3), np.uint8)

    img = cv2.line(pic, eye_l, eye_r, (0, 0, 250), thickness=3)
    img = cv2.line(img, acromion_l, acromion_r, (0, 0, 250), thickness=3)
    img = cv2.line(img, rib_cage_t, rib_cage_b, (0, 0, 250), thickness=3)
    img = cv2.line(img, asis_l, asis_r, (0, 0, 250), thickness=3)
    img = cv2.line(img, q_angle_r[0], q_angle_r[1], (0, 0, 250), thickness=3)
    img = cv2.line(img, q_angle_r[1], q_angle_r[2], (0, 0, 250), thickness=3)
    img = cv2.line(img, q_angle_l[0], q_angle_l[1], (0, 0, 250), thickness=3)
    img = cv2.line(img, q_angle_l[1], q_angle_l[2], (0, 0, 250), thickness=3)
    img = cv2.line(img, tibia_l[0], tibia_l[1], (0, 0, 250), thickness=3)
    img = cv2.line(img, tibia_r[0], tibia_r[1], (0, 0, 250), thickness=3)

    for i in range(len(points)):
        img = cv2.circle(img, points[i], 5, (250, 250, 0), -1)

    # img=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    # cv2.imwrite("/home/hossein/Desktop/tst.png",img)

    # img=cv2.resize(img,(165,220))

    return img


def f_checker(angles):
    res = {'head': "", 'shoulder': "", 'ribcage': "", 'rightknee': "", 'leftknee': "",
           'rightfoot': "", 'leftfoot': "", 'pelvis': ""}

    # print(angles)

    # *******Head**********
    if angles["head"] < (-5):
        res['head'] = "Tilted L"
    elif angles["head"] < (5):
        res['head'] = "Normal"
    else:
        res['head'] = "Tilted R"

    # *********Shoulder*******
    if angles["shoulder"] < (-5):
        res["shoulder"] = "Elevated L"
    elif angles["shoulder"] < (5):
        res["shoulder"] = "Level"
    else:
        res["shoulder"] = "Elevated R"

    # ********Rib cage*******
    if angles["ribcage"] < (-5):
        res["ribcage"] = "Rotated CW"
    elif angles["ribcage"] < (5):
        res["ribcage"] = "Normal"
    else:
        res["ribcage"] = "Rotated CCW"

    # ********Pelvis*******
    if angles["pelvis"] < (-5):
        res["pelvis"] = "Evaluated L"
    elif angles["pelvis"] < (5):
        res["pelvis"] = "Level"
    else:
        res["pelvis"] = "Evaluated R"

    # **********Left knee**********
    angl = angles["leftknee"]
    if angl < 0:
        if angl < (-15):
            res["leftknee"] = "Severe Genu Valgum"
        elif angl < (-7):
            res["leftknee"] = "Moderate Genu Valgum"
        else:
            res["leftknee"] = "Normal"

    else:
        if angl < 7:
            res["leftknee"] = "Normal"
        elif angl < 15:
            res["leftknee"] = "Moderate Genu Varum"
        else:
            res["leftknee"] = "Severe Genu Varum"

    # **********right knee**********
    angl = angles["rightknee"]
    if angl < 0:
        if angl < (-15):
            res["rightknee"] = "Severe Genu Valgum"
        elif angl < (-7):
            res["rightknee"] = "Moderate Genu Valgum"
        else:
            res["rightknee"] = "Normal"

    else:
        if angl < 7:
            res["rightknee"] = "Normal"
        elif angl < 15:
            res["rightknee"] = "Moderate Genu Varum"
        else:
            res["rightknee"] = "Severe Genu Varum"

    # **********left foot**********
    angl = angles["leftfoot"]
    if angl < (-5):
        res["leftfoot"] = "Internal Tibial Torsion"
    elif angl < 5:
        res["leftfoot"] = "Normal"
    else:
        res["leftfoot"] = "External Tibial Torsion"

    # **********right foot*********
    angl = angles["rightfoot"]
    if angl < (-5):
        res["rightfoot"] = "Internal Tibial Torsion"
    elif angl < 5:
        res["rightfoot"] = "Normal"
    else:
        res["rightfoot"] = "External Tibial Torsion"

    return res





def back_analyzer(points):
    if len(points) != 13:
        return "ERROR"

    scoliosis = [points[0], points[4], points[6]]
    acromion = [points[1], points[2]]
    scapula = [points[3], points[4]]
    psis = [points[7], points[8]]
    l_Achil = [points[9], points[10]]
    r_Achil = [points[11], points[12]]

    humeri = (acromion[1][0] - acromion[0][0]) / (acromion[1][1] - acromion[0][1])

    q1 = (scoliosis[1][0] - scoliosis[0][0]) / (scoliosis[1][1] - scoliosis[0][1])
    q2 = (scoliosis[2][0] - scoliosis[1][0]) / (scoliosis[2][1] - scoliosis[1][1])
    (qa1, qa2) = (np.arctan(q1), np.arctan(q2))
    (q1, q2) = (np.rad2deg(qa1), np.rad2deg(qa2))
    thoracic = (q2 - q1)

    scapulae = (scapula[1][0] - scapula[0][0]) / (scapula[1][1] - scapula[0][1])
    pelvis = (psis[1][0] - psis[0][0]) / (psis[1][1] - psis[0][1])

    R_foot = (r_Achil[1][1] - r_Achil[0][1]) / (r_Achil[1][0] - r_Achil[0][0])
    L_foot = (l_Achil[1][1] - l_Achil[0][1]) / (l_Achil[1][0] - l_Achil[0][0])

    # res = [humeri, thoracic, scapulae, pelvis, L_foot, R_foot]
    res={'humeri':int(humeri),'thoracic':int(thoracic),'scapulae':int(scapulae),
                   'pelvis': int(pelvis),'rightfoot':int(R_foot),'leftfoot':int(L_foot)}
    # print("res=",res)
    return res


def back_shaper(points):
    if len(points) != 15:
        return "ERROR"

    scoliosis = [points[10], points[11], points[12]]
    acromion = [points[0], points[1]]
    scapula = [points[2], points[3]]
    psis = [points[4], points[5]]
    l_Achil = [points[6], points[8]]
    r_Achil = [points[7], points[9]]

    img = np.zeros((640, 480, 3), np.uint8)

    img = cv2.line(img, scoliosis[0], scoliosis[1], (0, 0, 250), thickness=3)
    img = cv2.line(img, scoliosis[1], scoliosis[2], (0, 0, 250), thickness=3)
    img = cv2.line(img, acromion[0], acromion[1], (0, 0, 250), thickness=3)
    img = cv2.line(img, scapula[0], scapula[1], (0, 0, 250), thickness=3)
    img = cv2.line(img, psis[0], psis[1], (0, 0, 250), thickness=3)
    img = cv2.line(img, l_Achil[0], l_Achil[1], (0, 0, 250), thickness=3)
    img = cv2.line(img, r_Achil[0], r_Achil[1], (0, 0, 250), thickness=3)

    for i in range(len(points)):
        img = cv2.circle(img, points[i], 5, (250, 250, 0), -1)

    # img=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    # cv2.imwrite("/home/hossein/Desktop/tst.png",img)

    # img=cv2.resize(img,(165,220))

    return img


def b_checker(angls):
    res = {'humeri': "", 'thoracic': "", 'scapulae': "",
           'pelvis': "", 'rightfoot': "", 'leftfoot': ""}
    # ********humeri*********
    q = angls['humeri']
    if q < (-5):
        res['humeri'] = "Medially Rotated R"
    elif q < 5:
        res['humeri'] = "Normal"
    else:
        res['humeri'] = "Medially Rotated L"

    # ********thoracic*********
    q = abs(angls['thoracic'])
    if q < (10):
        res['thoracic'] = "Normal"
    elif q < 40:
        res['thoracic'] = "Moderate Scoliosis"
    else:
        res['thoracic'] = "Severe Scoliosis"

    # ********scapulae*********
    q = angls['scapulae']
    if q < (-5):
        res['scapulae'] = "Elevated R"
    elif q < 5:
        res['scapulae'] = "Normal"
    else:
        res['scapulae'] = "Elevated L"

    # ********pelvis*********
    q = angls['pelvis']
    if q < (-5):
        res['pelvis'] = "Rotated CW"
    elif q < 5:
        res['pelvis'] = "Level"
    else:
        res['pelvis'] = "Rotated CCW"

        # ********L foot*********
    q = angls['rightfoot']
    if q < (-5):
        res['rightfoot'] = "Supinated"
    elif q < 5:
        res['rightfoot'] = "Normal"
    else:
        res['rightfoot'] = "Pronated"

        # ********R foot*********
    q = angls['leftfoot']
    if q < (-5):
        res['leftfoot'] = "Supinated"
    elif q < 5:
        res['leftfoot'] = "Normal"
    else:
        res['leftfoot'] = "Pronated"

    return res



def side_shaper(points):
    cervical = [points[0], points[1]]
    kyphosis = [points[7], points[8]]
    lordosis = [points[9], points[10], points[11]]
    ankle = [points[5], points[6]]
    knee = [points[3], points[4], points[5]]
    hip = [points[2], points[3]]

    img = np.zeros((640, 480, 3), np.uint8)

    img = cv2.line(img, cervical[0], cervical[1], (0, 0, 250), thickness=3)
    img = cv2.line(img, kyphosis[0], kyphosis[1], (0, 0, 250), thickness=3)
    img = cv2.line(img, lordosis[0], lordosis[1], (0, 0, 250), thickness=3)
    img = cv2.line(img, lordosis[1], lordosis[2], (0, 0, 250), thickness=3)
    img = cv2.line(img, ankle[0], ankle[1], (0, 0, 250), thickness=3)
    img = cv2.line(img, knee[0], knee[1], (0, 0, 250), thickness=3)
    img = cv2.line(img, knee[1], knee[2], (0, 0, 250), thickness=3)
    img = cv2.line(img, hip[0], hip[1], (0, 0, 250), thickness=3)

    for i in range(len(points)):
        img = cv2.circle(img, points[i], 5, (250, 250, 0), -1)

    return img


def side_analyzer(points):

    cervical = [points[0], points[1]]
    kyphosis = [points[2], points[3]]
    lordosis = [points[4], points[5], points[6]]
    ankle = [points[10], points[11]]
    knee = [points[8], points[9], points[10]]
    hip = [points[7], points[8]]

    cervical_spine = (cervical[1][1] - cervical[0][1]) / (cervical[1][0] - cervical[0][0])
    Upper_thoracic = (kyphosis[1][1] - kyphosis[0][1]) / (kyphosis[1][0] - kyphosis[0][0])

    q1 = (lordosis[1][0] - lordosis[0][0]) / (lordosis[1][1] - lordosis[0][1])
    q2 = (lordosis[2][0] - lordosis[1][0]) / (lordosis[2][1] - lordosis[1][1])
    (qa1, qa2) = (np.arctan(q1), np.arctan(q2))
    (q1, q2) = (np.rad2deg(qa1), np.rad2deg(qa2))
    lumbar_spine = (q2 - q1)

    pelvis = (hip[1][0] - hip[0][0]) / (hip[1][1] - hip[0][1])

    q1 = (knee[1][0] - knee[0][0]) / (knee[1][1] - knee[0][1])
    q2 = (knee[2][0] - knee[1][0]) / (knee[2][1] - knee[1][1])
    (qa1, qa2) = (np.arctan(q1), np.arctan(q2))
    (q1, q2) = (np.rad2deg(qa1), np.rad2deg(qa2))
    knees = (q2 - q1)

    # res = [cervical_spine, Upper_thoracic, lumbar_spine, pelvis, knees]
    res={'cervicalspine':int(cervical_spine),'upperthoracic':int(Upper_thoracic),
                   'lumberspine':int(lumbar_spine),'pelvis':int(pelvis),'knees':int(knees)}
    return res


def s_checker(angls):
    res = {'cervicalspine': "", 'upperthoracic': "",
           'lumberspine': "", 'pelvis': "", 'knees': ""}
    # ********cervical spine*********
    q = angls['cervicalspine']
    if q < (0):
        res['cervicalspine'] = "Flat"
    elif q < 2:
        res['cervicalspine'] = "Exessive Extension"
    elif q < 10:
        res['cervicalspine'] = "Normal"
    else:
        res['cervicalspine'] = "Neck Forward"
    # ********Upper thoracic*********
    q = angls['upperthoracic']
    if q < (-40):
        res['upperthoracic'] = "Kyphosis"
    elif q < (-10):
        res['upperthoracic'] = "Normal"
    else:
        res['upperthoracic'] = "Flat"
    # ********lumber spine*********
    q = angls['lumberspine']
    if q < (5):
        res['lumberspine'] = "Flat"
    elif q < 40:
        res['lumberspine'] = "Normal"
    else:
        res['lumberspine'] = "Lordosis"
    # ********pelvis*********
    q = angls['pelvis']
    if q < (65):
        res['pelvis'] = "Posterior Pelvic Tilt"
    elif q < 75:
        res['pelvis'] = "Normal"
    else:
        res['pelvis'] = "Anterior Pelvic Tilt"
    # ********knees*********
    q = abs(angls['knees'])
    if q < (5):
        res['knees'] = "Normal"
    else:
        res['knees'] = "Flexed"

    return res
