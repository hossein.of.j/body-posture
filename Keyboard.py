from tkinter import *
from threading import Timer


class keyboard(Label):

    def load(self,bg):
        self.config(bg=bg)
        self.iter=0
        self.poss=[768-20*i for i in range(int((768-410)/20))]
        # print(self.poss)
        self.key_h=60
        self.key_w=60
        self.var=StringVar()
        self.key_poss=[[25+i*70 for i in range(13)]*3,[20]*13+[95]*13+[170]*13]

        self.key_poss[0][22] = self.key_poss[0][22] + 70
        self.key_poss[0][23] = self.key_poss[0][23] + 70
        self.key_poss[0][24] = self.key_poss[0][24] + 70
        for i in range(9):self.key_poss[0][13+i] = self.key_poss[0][13+i] + 35
        self.key_poss[0][33] = self.key_poss[0][33] + 3*70
        self.key_poss[0][34] = self.key_poss[0][34] + 3*70
        self.key_poss[0][35] = self.key_poss[0][35] + 3*70
        self.key_poss[0][36] = self.key_poss[0][33] + 70
        self.key_poss[1][36] = self.key_poss[1][35] + 70
        for i in range(7): self.key_poss[0][26 + i] = self.key_poss[0][26 + i] + 70

        self.key_poss[0][25] = self.key_poss[0][32]+140
        self.key_poss[1][25] = self.key_poss[1][32]

        self.key_poss[0][37] = self.key_poss[0][32]+70
        self.key_poss[1][37] = self.key_poss[1][32]



        self.key_words=["Q","W","E","R","T","Y","U","I","O","P","7","8","9",
                        "A","S","D","F","G","H","J","K","L","4","5","6","DEL",
                        "Z","X","C","V","B","N","M","1","2","3","0","Finish"]
        self.keys_loader()

        # self.handle=None
        self.place(x=0,y=768)

        return

    def refrencer(self,root):
        self.root=root

        return
    def placer(self):

        self.place(x=52,y=self.poss[self.iter], width=940, height=300)
        self.iter+=1
        if self.iter==len(self.poss):
            self.iter=0
            return

        Timer(0.02,self.placer).start()

        return

    def unplacer(self):
        self.s=self.place_info()

        if int(self.s['y'])>760: return
        i=len(self.poss)
        i=i-self.iter
        self.place(x=52, y=self.poss[i-1], width=940, height=300)
        self.iter += 1
        if self.iter == len(self.poss):
            self.iter = 0
            return

        Timer(0.02, self.unplacer).start()

        return

    def finit(self):
        self.root.main_sorter()
        self.unplacer()
        return
    def keys_loader(self):
        self.keys=[Label(self,bg="#a5aee7") for i in range(38)]
        for i in range(38): self.keys[i].config(text=self.key_words[i])
        for i in range(38): self.keys[i].config(font=('Arial',20,'bold'))
        self.keys[37].config(font=('Arial',16,'bold'))
        for i in range(38): self.keys[i].place(x=self.key_poss[0][i],y=self.key_poss[1][i],width=self.key_w,height=self.key_h)

        self.keys[0].bind("<1>", lambda x: self.adder(self.key_words[0]))
        self.keys[1].bind("<1>", lambda x: self.adder(self.key_words[1]))
        self.keys[2].bind("<1>", lambda x: self.adder(self.key_words[2]))
        self.keys[3].bind("<1>", lambda x: self.adder(self.key_words[3]))
        self.keys[4].bind("<1>", lambda x: self.adder(self.key_words[4]))
        self.keys[5].bind("<1>", lambda x: self.adder(self.key_words[5]))
        self.keys[6].bind("<1>", lambda x: self.adder(self.key_words[6]))
        self.keys[7].bind("<1>", lambda x: self.adder(self.key_words[7]))
        self.keys[8].bind("<1>", lambda x: self.adder(self.key_words[8]))
        self.keys[9].bind("<1>", lambda x: self.adder(self.key_words[9]))
        self.keys[10].bind("<1>", lambda x: self.adder(self.key_words[10]))
        self.keys[11].bind("<1>", lambda x: self.adder(self.key_words[11]))
        self.keys[12].bind("<1>", lambda x: self.adder(self.key_words[12]))
        self.keys[13].bind("<1>", lambda x: self.adder(self.key_words[13]))
        self.keys[14].bind("<1>", lambda x: self.adder(self.key_words[14]))
        self.keys[15].bind("<1>", lambda x: self.adder(self.key_words[15]))
        self.keys[16].bind("<1>", lambda x: self.adder(self.key_words[16]))
        self.keys[17].bind("<1>", lambda x: self.adder(self.key_words[17]))
        self.keys[18].bind("<1>", lambda x: self.adder(self.key_words[18]))
        self.keys[19].bind("<1>", lambda x: self.adder(self.key_words[19]))
        self.keys[20].bind("<1>", lambda x: self.adder(self.key_words[20]))
        self.keys[21].bind("<1>", lambda x: self.adder(self.key_words[21]))
        self.keys[22].bind("<1>", lambda x: self.adder(self.key_words[22]))
        self.keys[23].bind("<1>", lambda x: self.adder(self.key_words[23]))
        self.keys[24].bind("<1>", lambda x: self.adder(self.key_words[24]))
        self.keys[25].bind("<1>", lambda x: self.DEL())
        self.keys[26].bind("<1>", lambda x: self.adder(self.key_words[26]))
        self.keys[27].bind("<1>", lambda x: self.adder(self.key_words[27]))
        self.keys[28].bind("<1>", lambda x: self.adder(self.key_words[28]))
        self.keys[29].bind("<1>", lambda x: self.adder(self.key_words[29]))
        self.keys[30].bind("<1>", lambda x: self.adder(self.key_words[30]))
        self.keys[31].bind("<1>", lambda x: self.adder(self.key_words[31]))
        self.keys[32].bind("<1>", lambda x: self.adder(self.key_words[32]))
        self.keys[33].bind("<1>", lambda x: self.adder(self.key_words[33]))
        self.keys[34].bind("<1>", lambda x: self.adder(self.key_words[34]))
        self.keys[35].bind("<1>", lambda x: self.adder(self.key_words[35]))
        self.keys[36].bind("<1>", lambda x: self.adder(self.key_words[36]))
        self.keys[37].bind("<1>", lambda x: self.finit())





        return


    def adder(self,st):
        str=self.var.get()
        str=str+st
        self.var.set(str)
        return

    def DEL(self):
        str=self.var.get()
        str=str[:-1]
        self.var.set(str)
        return