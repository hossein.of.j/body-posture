from tkinter import *
import sys, cv2
import numpy as np
import calculator as calc
from PIL import ImageTk, Image
import os
# import thermal as tp
import pic_maker
import hostapd_emulator as host_apd


class form:
    def __init__(self, master):
        self.path = sys.path[0]

        self.bg_pic = PhotoImage(file=self.path + "/pics/report/bg.png")
        self.bg = Label(master, image=self.bg_pic)
        self.bg.bind("<Button-1>", self.clicking)

        self.btn_pics = [PhotoImage(file=self.path + "/pics/report/home.png"),
                         PhotoImage(file=self.path + "/pics/report/save.png"),
                         PhotoImage(file=self.path + "/pics/report/delete.png"),
                         PhotoImage(file=self.path + "/pics/report/print.png"),
                         PhotoImage(file=self.path + "/pics/report/send.png")]

        self.btns = [Label(self.bg, image=self.btn_pics[i], bg="#89abb5") for i in range(5)]
        self.btns[0].place(x=803, y=145)
        #self.btns[1].place(x=803, y=290)
        self.btns[2].place(x=803, y=290)
        self.btns[3].place(x=803, y=435)
        self.btns[4].place(x=803, y=585)
        self.btns[0].bind("<Button-1>", lambda x: self.hide())
        self.btns[1].bind("<Button-1>", lambda x: self.saver())
        self.btns[2].bind("<Button-1>", lambda x: self.dell_all())
        self.btns[3].bind("<Button-1>", lambda x: tp.call(self.res,self.informs[0],self.informs[1],self.informs[2]))
        self.btns[4].bind("<Button-1>", lambda x: self.send_func())

        self.res = [{'head': (0, "Moderate"), 'shoulder': (0, "Moderate"), 'ribcage': (0, "Moderate"),
                     'pelvis': (0, "Moderate"),
                     'leftknee': (0, "Moderate"), 'rightknee': (0, "Moderate"), 'leftfoot': (0, "Moderate"),
                     'rightfoot': (0, "Moderate")},
                    {'cervicalspine': (0, "Moderate"), 'upperthoracic': (0, "Moderate"),
                     'lumberspine': (0, "Moderate"), 'pelvis': (0, "Moderate"), 'knees': (0, "Moderate")},
                    {'humeri': (0, "Moderate"), 'thoracic': (0, "Moderate"), 'scapulae': (0, "Moderate"),
                     'pelvis': (0, "Moderate"), 'rightfoot': (0, "Moderate"), 'leftfoot': (0, "Moderate")}]

        self.fnt = ("Arial", 20, 'bold')

        self.lbs = [[Label(self.bg, bg="#89abb5"), Label(self.bg, bg="#89abb5")],
                    [Label(self.bg, bg="white"), Label(self.bg, bg="white")],
                    [Label(self.bg, bg="#89abb5"), Label(self.bg, bg="#89abb5")],
                    [Label(self.bg, bg="white"), Label(self.bg, bg="white")],
                    [Label(self.bg, bg="#89abb5"), Label(self.bg, bg="#89abb5")],
                    [Label(self.bg, bg="white"), Label(self.bg, bg="white")],
                    [Label(self.bg, bg="#89abb5"), Label(self.bg, bg="#89abb5")],
                    [Label(self.bg, bg="white"), Label(self.bg, bg="white")],
                    [Label(self.bg, bg="#89abb5"), Label(self.bg, bg="#89abb5")],
                    [Label(self.bg, bg="white"), Label(self.bg, bg="white")]]

        for i in range(10):self.lbs[i][0].place(x=255,y=103+i*63,width=183,height=50)
        for i in range(10): self.lbs[i][1].place(x=455, y=103 + i * 63, width=325, height=50)

        for i in range(10):
            self.lbs[i][0].config(font = self.fnt)
            self.lbs[i][1].config(font = self.fnt)

            self.lbs[i][0].config(text="****")
            self.lbs[i][1].config(text="**********")

        self.lbs[8][1].config(font=('Arial',14,'bold'))
        self.lbs[7][1].config(font=('Arial', 13, 'bold'))

        self.send_fr_col="#92C2C5"

        self.send_fr=Frame(self.bg,width=600,height=400,bg=self.send_fr_col)
        self.s_fr_p=[PhotoImage(file=self.path + "/pics/report/m_1.png"),
                     PhotoImage(file=self.path + "/pics/report/m_2.png"),
                     PhotoImage(file=self.path + "/pics/report/m_0.png")]
        self.s_fr_p_l=[Label(self.send_fr, image=self.s_fr_p[0],bg='black'),
                       Label(self.send_fr, image=self.s_fr_p[1],bg='black'),
                       Label(self.send_fr, image=self.s_fr_p[2],bg='black')]

        self.s_fr_p_l[0].place(x=100, y=10)
        self.s_fr_p_l[1].place(x=260, y=10)
        self.s_fr_p_l[2].place(x=420, y=10)
        self.p_mod=IntVar()
        self.p_mod.set(1)
        self.s_fr_p_l[0].bind("<Button-1>", lambda x: self.p_mod.set(1))
        self.s_fr_p_l[1].bind("<Button-1>", lambda x: self.p_mod.set(2))
        self.s_fr_p_l[2].bind("<Button-1>", lambda x: self.p_mod.set(0))
        self.p_mod_r=[Radiobutton(self.send_fr, variable=self.p_mod, value=1,bg=self.send_fr_col,highlightthickness=0),
                      Radiobutton(self.send_fr, variable=self.p_mod, value=2,bg=self.send_fr_col,highlightthickness=0),
                      Radiobutton(self.send_fr, variable=self.p_mod, value=0,bg=self.send_fr_col,highlightthickness=0)]
        self.p_mod_r[0].place(x=125, y=220)
        self.p_mod_r[1].place(x=300-15, y=220)
        self.p_mod_r[2].place(x=460-15, y=220)

        self.s_fr_btns=[Label(self.send_fr,text="Ext Printer",bg="#76D582",anchor="center",font=('Arial',14)),
                        Label(self.send_fr, text="WiFi", bg="#76D582",anchor="center",font=('Arial',18))]

        self.s_fr_btns[0].place(x=130,y=300,width=120,height=60)
        self.s_fr_btns[1].place(x=330, y=300, width=120, height=60)
        self.s_fr_btns[1].bind("<Button-1>",lambda x : self.wifi())
        self.s_fr_btns[0].bind("<Button-1>",lambda x : self.printer())

        self.s_fr_cncl = Label(self.send_fr,text="Cancel",bg="#87CDA4",font=('Arial',18))
        self.s_fr_cncl.place(x=5,y=5,width=80,height=45)
        self.s_fr_cncl.bind("<Button-1>",lambda x: self.send_fr.place_forget())

        # self.send_fr.place(x=(1024-600)/2,y=(768-400)/2)


        # self.tst = Label(self.bg, bg="blue").place(x=900, y=635, width=100, height=100)
        return
    def saver(self):
        #self.saver_on=True
        return
    
    def show(self, forms, inf):

        #self.saver_on=False
        self.forms = forms
        self.informs = inf

        self.loading()
        res=self.calculator()
        if res!=True: return res


        self.lbl_puter()
        # self.pic_maker()

        self.bg.place(x=0, y=0, width=1024, height=768)
        return True
    def dell_all(self):
        cmd=self.path+"/Histories/"+self.informs[2]+"/"+self.informs[0]+":"+self.informs[1]
        cmd="rm -r "+cmd
        os.system(cmd)
        print(cmd)


        self.hide()
        return
    def hide(self):
            
        self.bg.place_forget()

    def clicking(self, event):
        x = event.x
        y = event.y
        x = event.x
        y = event.y

        if (x < 120) & (y < 120):
            self.ask()
        elif (x > 800) & (x < 900) & (y > 635) & (y < 735):
            self.analys()
        elif (x > 900) & (x < 1000) & (y > 635) & (y < 735):
            self.print()

    def loading(self):
        self.fldr_name = self.informs[0] + ":" + self.informs[1]  # + ":" + self.informs[3]
        self.pics = ["", "", ""]
        for i in range(3):
            self.pics[i] = cv2.imread(
                self.path + "/Histories/" + self.informs[2] + "/" + self.fldr_name + "/no_" + str(i) + ".png", 1)
            self.pic_h, self.pic_w, c = self.pics[i].shape
            pass

        self.mark_check()
        # print("loaded")

    def mark_check(self):
        f = open(self.path + "/Histories/" + self.informs[2] + "/" + self.fldr_name + "/anal.txt", 'r')
        st = f.read()
        s = st.split('--')
        del s[0]
        del s[0]
        del s[1]
        del s[2]
        self.marks = [[], [], []]
        for i in range(3):
            fs = s[i].split(":")
            del fs[0]
            for k in range(len(fs)):
                ss = fs[k].split(',')
                self.marks[i].append((int(ss[0]), int(ss[1])))
                # print("mark--",i,"--",self.marks[i])
                # print("marked")

    def calculator(self):
        # print("calc")
        # self.f_skeleton=calc.front_shaper(self.marks[0])
        try:
            self.front_angl = calc.front_analyzer(self.marks[0])
            self.f_res = calc.f_checker(self.front_angl)
        except ZeroDivisionError:
            print("Front Mark Problem")
            return 'F'
            
        # self.b_skeleton = calc.back_shaper(self.marks[1])
        try:
            self.back_angl = calc.back_analyzer(self.marks[2])
            self.b_res = calc.b_checker(self.back_angl)
        except ZeroDivisionError:
            print("Back Mark Problem")
            return 'B'
        
        # self.s_skeleton=calc.side_shaper(self.marks[2])
        try:
            self.side_angl = calc.side_analyzer(self.marks[1])
            self.s_res = calc.s_checker(self.side_angl)
        except ZeroDivisionError:
            print("Side Mark Problem")
            return 'S'
        

        self.res_putter()

        return True

    def res_putter(self):
        for i in self.res[0]:
            h = (round(self.front_angl[i],1), self.f_res[i])
            self.res[0][i] = h
        for i in self.res[1]:
            h = (round(self.side_angl[i],1), self.s_res[i])
            self.res[1][i] = h

        for i in self.res[2]:
            h = (round(self.back_angl[i],1), self.b_res[i])
            self.res[2][i] = h



        return

    def pic_maker(self):

        self.f_mask = cv2.cvtColor(self.f_skeleton, cv2.COLOR_BGR2GRAY)
        rett, self.f_mask = cv2.threshold(self.f_mask, 10, 255, cv2.THRESH_BINARY)
        self.f_mask_inv = cv2.bitwise_not(self.f_mask)
        self.f_skeleton = cv2.bitwise_and(self.f_skeleton, self.f_skeleton, mask=self.f_mask)
        self.pics[0] = cv2.bitwise_and(self.pics[0], self.pics[0], mask=self.f_mask_inv)
        self.pics[0] = cv2.add(self.pics[0], self.f_skeleton)

        self.b_mask = cv2.cvtColor(self.b_skeleton, cv2.COLOR_BGR2GRAY)
        rett, self.b_mask = cv2.threshold(self.b_mask, 10, 255, cv2.THRESH_BINARY)
        self.b_mask_inv = cv2.bitwise_not(self.b_mask)
        self.b_skeleton = cv2.bitwise_and(self.b_skeleton, self.b_skeleton, mask=self.b_mask)
        self.pics[1] = cv2.bitwise_and(self.pics[1], self.pics[1], mask=self.b_mask_inv)
        self.pics[1] = cv2.add(self.pics[1], self.b_skeleton)

        self.s_mask = cv2.cvtColor(self.s_skeleton, cv2.COLOR_BGR2GRAY)
        rett, self.s_mask = cv2.threshold(self.s_mask, 10, 255, cv2.THRESH_BINARY)
        self.s_mask_inv = cv2.bitwise_not(self.s_mask)
        self.s_skeleton = cv2.bitwise_and(self.s_skeleton, self.s_skeleton, mask=self.s_mask)
        self.pics[2] = cv2.bitwise_and(self.pics[2], self.pics[2], mask=self.s_mask_inv)
        self.pics[2] = cv2.add(self.pics[2], self.s_skeleton)

        b, g, r = cv2.split(self.pics[0])
        b1, g1, r1 = cv2.split(self.pics[1])
        b2, g2, r2 = cv2.split(self.pics[2])
        self.img = [cv2.merge((r, g, b)), cv2.merge((r1, g1, b1)), cv2.merge((r2, g2, b2))]
        for i in range(3):
            self.img[i] = cv2.resize(self.img[i], (165, 220))
            self.img[i] = Image.fromarray(self.img[i])
            self.img[i] = ImageTk.PhotoImage(self.img[i])
            self.pics_lb[i].config(image=self.img[i])


    def lbl_puter(self):

        needed_lbs=[self.res[1]["cervicalspine"],self.res[0]['shoulder'],self.res[1]['upperthoracic'],self.res[1]['lumberspine'],
                    self.res[2]['scapulae'],self.res[2]['thoracic'],self.res[0]["pelvis"]]

        for i in range(len(needed_lbs)):
            self.lbs[i][0].config(text=str(round(needed_lbs[i][0],1)))
            self.lbs[i][1].config(text=needed_lbs[i][1])

        ##tibia
        self.lbs[7][0].config(
            text=str(round(self.res[0]['rightfoot'][0], 1)) + "/" + str(round(self.res[0]['leftfoot'][0], 1)))
        self.lbs[7][1].config(
            text=self.res[0]['rightfoot'][1] + "/" + self.res[0]['leftfoot'][1])


        ##knee
        self.lbs[8][0].config(text=str(round(self.res[0]['rightknee'][0], 1))+"/"+str(round(self.res[0]['leftknee'][0], 1)))
        self.lbs[8][1].config(
            text=self.res[0]['rightknee'][1] + "/" +self.res[0]['leftknee'][1])
        ##foot
        self.lbs[9][0].config(
            text=str(round(self.res[2]['rightfoot'][0], 1)) + "/" + str(round(self.res[2]['leftfoot'][0], 1)))
        self.lbs[9][1].config(
            text=self.res[2]['rightfoot'][1] + "/" + self.res[2]['leftfoot'][1])




        return






    def analys(self):
        self.forms[5].show(self.forms, self.informs)
        self.hide()

    def print(self):
        print("print")

    def ask(self):
        self.fr = Frame(self.bg, width=300, height=200, bg="gray")
        self.lbq = Label(self.fr, text="Are You Sure?", bg="gray", font=("Arial", 30, 'bold'))
        self.lbq.place(x=30, y=80)
        self.btn0 = Button(self.fr, text="YES", command=self.yesi)
        self.btn1 = Button(self.fr, text="NO", command=self.noi)
        self.btn0.place(x=130, y=120)
        self.btn1.place(x=200, y=120)

        self.fr.place(x=(1024 - 300) / 2, y=(768 - 200) / 2)

    def yesi(self):
        self.fr.place_forget()
        self.hide()
        return

    def noi(self):
        self.fr.place_forget()
        return

    def send_func(self):
        self.send_fr.place(x=(1024 - 600) / 2, y=(768 - 400) / 2)

        #pic_maker.Maker(0,self.informs,self.res)

        return

    def wifi(self):
        self.html_maker()
        host_apd.start()
        print("hotspot on")

        self.result_pic=pic_maker.Maker(self.p_mod.get(),self.informs,self.res)
        cv2.imwrite("/var/www/html/"+self.informs[0]+".png",self.result_pic)
        print("mode = ",self.p_mod.get())
        self.Wifi_lb=Label(self.send_fr,text="connect to Body_posture WiFi\n pass: 123456789\n\n goto 192.168.1.1\n\n download the result",
                           anchor='center',font=('Arial',28,''),fg="black",bg=self.send_fr_col)
        self.Wifi_lb.place(x=0,y=0,width=600,height=400)
        self.Wifi_lb.bind("<Button-1>",lambda x: self.wifi_off())

        return
    def wifi_off(self):
        
        self.Wifi_lb.place_forget()
        host_apd.stop()
        print("hotspot off")
        return
    def printer(self):
        self.result_pic = pic_maker.Maker(self.p_mod.get(), self.informs, self.res)
        cv2.imwrite("res.png",self.result_pic)
        cmd="lp res.png"
        self.Printer_lb = Label(self.send_fr,
                             text="Result Page is Printing . . . ",
                             anchor='center', font=('Arial', 28, ''), fg="black", bg=self.send_fr_col)
        self.Printer_lb.place(x=0, y=0, width=600, height=400)
        self.Printer_lb.bind("<Button-1>", lambda x: self.print_off())

        self.send_fr.after(100,lambda :print(os.system(cmd)))


        return cmd

    def print_off(self):


        self.Printer_lb.place_forget()


    def html_maker(self):

        string='<html><header></header><body><img src="'+self.informs[0]+'.png" alt="'+self.informs[0]+'" style="width:800px;height:1131px"></body></html>'
        f=open("/var/www/html/index.html",'w')
        f.write(string)
        f.close()


        
