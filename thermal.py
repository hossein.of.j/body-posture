import thermalprinter
from PIL import Image
from time import sleep
tp=thermalprinter.ThermalPrinter('/dev/serial0')

re = [{'head': (0, "Moderate"), 'shoulder': (0, "Moderate"), 'ribcage': (0, "Moderate"),
                     'pelvis': (0, "Moderate"),
                     'leftknee': (0, "Moderate"), 'rightknee': (0, "Moderate"), 'leftfoot': (0, "Moderate"),
                     'rightfoot': (0, "Moderate")},
                    {'cervicalspine': (0, "Moderate"), 'upperthoracic': (0, "Moderate"),
                     'lumberspine': (0, "Moderate"), 'pelvis': (0, "Moderate"), 'knees': (0, "Moderate")},
                    {'humeri': (0, "Moderate"), 'thoracic': (0, "Moderate"), 'scapulae': (0, "Moderate"),
                     'pelvis': (0, "Moderate"), 'rightfoot': (0, "Moderate"), 'leftfoot': (0, "Moderate")}]


s_t=0.5

def call(res,Name,Id,Date):
    onvan="Item        val(deg)      result"
    tp.feed(1)



    tp.out("_________",line_feed=True,justify='C',bold=True)    
    sleep(s_t)
    tp.out("   DSI   ",justify='C',size='L',bold=True,line_spacing=45,underline=2)    


    sleep(s_t*1.3)

    tp.feed(1)

    tp.out("Name : ",line_feed=False)
    tp.out(Name)

    tp.out("ID   : ",line_feed=False)
    tp.out(Id)
    
    tp.out("Date : ",line_feed=False)
    tp.out(Date)
    
    
    sleep(s_t*1.3)


    tp.feed(1)
    tp.out("< Front >",justify='C',bold=True)
    
    sleep(s_t)
    tp.feed(1)

    tp.out(onvan,bold=True)
    tp.out(":"*32,line_feed=True,justify='C',bold=True)    
    tp.feed(1)
    
    tp.out("head      :",justify='L',line_feed=False)
    tp.out("    "+str(res[0]['head'][0])+"  ")
    tp.out(str(res[0]['head'][1]),justify='R')

    tp.out("shoulder  :",justify='L',line_feed=False)
    tp.out("    "+str(res[0]['shoulder'][0])+"  ")
    tp.out(str(res[0]['shoulder'][1]),justify='R')

    tp.out("ribcage   :",justify='L',line_feed=False)
    tp.out("    "+str(res[0]['ribcage'][0])+"  ")
    tp.out(str(res[0]['ribcage'][1]),justify='R')

    tp.out("pelvis    :",justify='L',line_feed=False)
    tp.out("    "+str(res[0]['pelvis'][0])+"  ")
    tp.out(str(res[0]['pelvis'][1]),justify='R')

    tp.out("left Knee :",justify='L',line_feed=False)
    tp.out("    "+str(res[0]['leftknee'][0])+"  ")
    tp.out(str(res[0]['leftknee'][1]),justify='R')

    tp.out("Right Knee:",justify='L',line_feed=False)
    tp.out("    "+str(res[0]['rightknee'][0])+"  ")
    tp.out(str(res[0]['rightknee'][1]),justify='R')


    tp.out("Left Foot :",justify='L',line_feed=False)
    tp.out("    "+str(res[0]['leftfoot'][0])+"  ")
    tp.out(str(res[0]['leftfoot'][1]),justify='R')

    tp.out("Right Foot:",justify='L',line_feed=False)
    tp.out("    "+str(res[0]['rightfoot'][0])+"  ")
    tp.out(str(res[0]['rightfoot'][1]),justify='R')

    sleep(s_t*1.3)
    
    tp.feed(1)
    tp.out("< Side >",justify='C',bold=True)
    sleep(s_t)

    tp.feed(1)
    tp.out(onvan,bold=True)
    tp.out(":"*32,line_feed=True,justify='C',bold=True)
    tp.feed(1)
    
    tp.out("CervicalSpine :",justify='L',line_feed=False)
    tp.out("    "+str(res[1]['cervicalspine'][0])+"  ")
    tp.out(str(res[1]['cervicalspine'][1]),justify='R')

    tp.out("Upper thoracic:",justify='L',line_feed=False)
    tp.out("    "+str(res[1]['upperthoracic'][0])+"  ")
    tp.out(str(res[1]['upperthoracic'][1]),justify='R')

    tp.out("LumberSpine   :",justify='L',line_feed=False)
    tp.out("    "+str(res[1]['lumberspine'][0])+"  ")
    tp.out(str(res[1]['lumberspine'][1]),justify='R')

    tp.out("Pelvis        :",justify='L',line_feed=False)
    tp.out("    "+str(res[1]['pelvis'][0])+"  ")
    tp.out(str(res[1]['pelvis'][1]),justify='R')

    tp.out("Knees         :",justify='L',line_feed=False)
    tp.out("    "+str(res[1]['knees'][0])+"  ")
    tp.out(str(res[1]['knees'][1]),justify='R')



    sleep(s_t*1.3)
    
    tp.feed(1)
    tp.out("< Back >",justify='C',bold=True)
    sleep(s_t)

    tp.feed(1)
    tp.out(onvan,bold=True)
    tp.out(":"*32,line_feed=True,justify='C',bold=True)    
    tp.feed(1)
    tp.out("humeri    :",justify='L',line_feed=False)
    tp.out("    "+str(res[2]['humeri'][0])+"  ")
    tp.out(str(res[2]['humeri'][1]),justify='R')

    tp.out("thoracic  :",justify='L',line_feed=False)
    tp.out("    "+str(res[2]['thoracic'][0])+"  ")
    tp.out(str(res[2]['thoracic'][1]),justify='R')

    tp.out("Scapulae  :",justify='L',line_feed=False)
    tp.out("    "+str(res[2]['scapulae'][0])+"  ")
    tp.out(str(res[2]['scapulae'][1]),justify='R')

    tp.out("pelvis    :",justify='L',line_feed=False)
    tp.out("    "+str(res[2]['pelvis'][0])+"  ")
    tp.out(str(res[2]['pelvis'][1]),justify='R')

    tp.out("Right foot:",justify='L',line_feed=False)
    tp.out("    "+str(res[2]['rightfoot'][0])+"  ")
    tp.out(str(res[2]['rightfoot'][1]),justify='R')

    tp.out("Left foot :",justify='L',line_feed=False)
    tp.out("    "+str(res[2]['leftfoot'][0])+"  ")
    tp.out(str(res[2]['leftfoot'][1]),justify='R')



    tp.feed(4)



